This repository contains simulation of NAO humanoid robot with artificial skin sensors and some experiments in autonomous self-exploration.

# Table of Contents

- [Table of Contents](#table-of-contents)
- [Installation instructions](#installation-instructions)
  - [Software prerequisites:](#software-prerequisites)
  - [Installation of required packages](#installation-of-required-packages)
    - [ROS packages](#ros-packages)
    - [Explauto](#explauto)
    - [Files from this repository](#files-from-this-repository)
  - [Final steps](#final-steps)
- [Usage instructions](#usage-instructions)
  - [Starting the simulation](#starting-the-simulation)
  - [Manual control](#manual-control)
  - [Running self-exploration experiments](#running-self-exploration-experiments)
  - [Getting position of end-effectors in robot arms](#getting-position-of-end-effectors-in-robot-arms)
  - [Getting taxel positions](#getting-taxel-positions)
- [Changing robot model description](#changing-robot-model-description)

<a name="inst_instr"></a>
# Installation instructions

## Software prerequisites:

- [Ubuntu 18.04 Bionic Beaver](https://help.ubuntu.com/lts/installation-guide/)
- [ROS Melodic](http://wiki.ros.org/melodic/Installation)
- [Gazebo 9](http://gazebosim.org/tutorials?cat=install&tut=install_ubuntu&ver=9.0)

<a name="inst_req"></a>
## Installation of required packages

### ROS packages

Some NAO ROS packages are still not ported for ROS Melodic. Install the following packages into Catkin workspace by cloning from GitHub:

- [nao_robot](https://github.com/ros-naoqi/nao_robot)
- [roboticsgroup_gazebo_plugins](https://github.com/roboticsgroup/roboticsgroup_gazebo_plugins)
- [rqt_ez_publisher](https://github.com/OTL/rqt_ez_publisher)

You will also need packages [nao_meshes](https://github.com/ros-naoqi/nao_meshes) and [nao_virtual](https://github.com/ros-naoqi/nao_virtual). When I was working on my project, these packages were not compatible with ROS Melodic and I had to modify them. You **should** use ZIP archives in this repository, in folder `./gazebo_9/catkin_ws/src/`. Unpack them to `/src/` inside your Catkin workspace. In January 2020, a version of `nao_meshes` for ROS Melodic was finally officially released, but I did not yet test it, better simply use the version from ZIP archives in this repository.

<a name="inst_req_explauto"></a>
### Explauto

Explauto library is used to conduct autonomous self-exploration in this project. Explauto source code and documentation can be found in the [github repository](https://github.com/flowersteam/explauto). Explauto can be installed using python `pip` command:

```
pip install explauto
```

For one of the experiments (involving SAGG-RIAC exploration strategy), I had to modify the library code to plot adaptive subdivision tree data structure to file. Add the following code to file `tree.py` (you can find the location of the library files with `pip show explauto`):

```
def plot_to_file(self, fh, progress_max=1., depth=10, plot_dims=[0,1]):
    if self.leafnode or depth == 0:

        mins = self.bounds_x[0,plot_dims]
        maxs = self.bounds_x[1,plot_dims]

        prog_min = 0.
        p = (self.max_leaf_progress - prog_min) / (progress_max - prog_min) if progress_max > prog_min else 0
        fh.write(str(mins[0]) + ' ' + str(mins[1]) + ' ' + str(maxs[0]) + ' ' + str(maxs[1]) + ' ' + str(p) + '\n')
        #ax.add_patch(plt.Rectangle(mins, maxs[0] - mins[0], maxs[1] - mins[1], color=c, alpha=0.7))

    else:
        self.lower.plot_to_file(fh, progress_max, depth - 1, plot_dims)
        self.greater.plot_to_file(fh, progress_max, depth - 1, plot_dims)
```

For experiments involving `CMA-ES` optimization method, Explauto Library produced error. I had to fix it by modifying library file `sensorimotor_model/inverse/cma.py` around line 957 (commented lines of code produced error):

```
def has_bounds(self):
    """return True, if any variable is bounded"""
    bounds = self.bounds
    #if bounds in (None, [None, None]):
    #    return False
    for ib, bound in enumerate(bounds):
        if bound is not None:
            sign_ = 2 * ib - 1
            for bound_i in bound:
                if bound_i is not None and sign_ * bound_i < np.inf:
                    return True
    return False
```

<a name="inst_req_rep"></a>
### Files from this repository

After installing the packages, copy following folders and files from this repository `./gazebo_9/catkin_ws/src/` to your Catkin workspace:

- `gazebo_contactsensor_plugin` contains plugin for detecting skin contact in the simulated robot. You have to build it manually:
```
cd catkin_ws/src/gazebo_contactsensor_plugin
mkdir build
cd build
cmake ..
make
```
- `nao_explauto` contains python scripts that perform simulated experiments on the robot
- `nao_robot` contains modified version of robot body in URDF format and launch files that initiate Gazebo 9 simulation

Contents of other folders in this repository (these are not very important):

- `gazebo-meshes` - `stl` and `dae` files for plastic housing on which the artificial skin is mounted
- `local-frame-skin-coordinates` - coordinate transforms for exporting meshes from CAD files
- `matlab-graphs` - MATLAB code for graphs that I used in my bachelor's thesis
- `misc` - files used to run the simulation, see next section
- `skin-generation` - scripts that generate URDF code for artificial skin from coordinates exported from CAD files

<a name="inst_final"></a>
## Final steps

After all packages have been installed and all files from this repository were copied to catkin workspace, you need to run `catkin_make` command to build the packages and plugins.

On the latest version (if running the experiments from the `tcds` folder or more recent), it is recommended to install the `scrot` package, through `sudo apt install scrot` to be able to use the feature that takes screenshots of gazebo during the testing phase.
However, this has been added with compatibility with ubuntu 18.04 in mind. It may not work with another OS, and if using a different version of ubuntu, check the corresponding official documentation of `scrot` to modify the code and ensure the intended behaviour if necessary.

<a name="usage_instr"></a>
# Usage instructions

## Starting the simulation

The simulation can be started by using bash script `./gazebo9/misc/launch-naoqi.sh`. Put it into the root of your catkin workspace directory. The script is configurable, you can select `-v`, acceptable values are `Skin` (low resolution version) and `Skin2` (high resolution version). You can also turn off and on contact detection on different body parts using command line parameters `-t` for torso, `-h` for head, `-l` for left and `-r` for right arm, acceptable values are `true` and `false`. To turn off completely collision with arms (to guarantee there are no unwanted contacts), set command line argument `-f` to `true`.

You can also change physics engine used in the simuation with `-p`. Default physics engine is `ode`, other possibilities are `dart` and `bullet`.

Example usage:

```
./launch-naoqi.sh -v Skin2 -t false -h true -l false -r true -p dart -f true
```

**Note:** for some unknown reason, sometimes Gazebo will not load NAO controllers. If that happens, just stop Gazebo and start it again.

<a name="usage_manual"></a>
## Manual control

After starting the simulation, you can control the robot manually using `rqt_ez_publisher` ROS package:
```
rosrun rqt_ez_publisher rqt_ez_publisher --force-discover
```
After starting the UI, load settings from file `rqt_ez_publisher.yaml`

<a name="usage_run"></a>
## Running self-exploration experiments

After starting the Gazebo simulation, unpause it and run one of the experiments from `./gazebo9/catkin_ws/src/nao_explauto/python/` directory. Contents of files and directories:

- `coordinate-maps` contains mappings from symbolic node numbers to coordinates on 2D skin projection
- `experiments-done` contains several experiments that I have performed for my thesis
- `models` and `output` directories contain results of last performed experiment
- `gazebo_link_pose.py` is a class that gets link pose from `/gazebo/link_states` ROS topic
- `nao_explauto_env.py` implements Explauto environment that maps contact point to binary string
- `nao_explauto_env_2.py` implements Explauto environment that maps contact points to planar projection
- `nao_ml_inc.py` implements functions that get joint states and set NAO pose in Gazebo simulator, by reading from and publishing to corresponding ROS topics
- `reset-pose-torso.py` and `test-explauto.py` are scripts that serve testing purposes

<a name="usage_3dpos"></a>
## Getting position of end-effectors in robot arms

Two invisible links were added to the contact tips of "pens" in robot arms. The links
are named `l_wrist_pen_point` and `r_wrist_pen_point`. To get the position of the
tip of an end effector "pen" relative to the world frame from command line, run
the following command:

```
rosservice call /gazebo/get_link_state '{link_name: "nao_skin::l_wrist_pen_point", reference_frame: world}'
```

An example of getting this position from a python script is in
`./gazebo9/catkin_ws/src/nao_explauto/python/end_effector_position.py`

<a name="usage_taxelpos"></a>
## Getting taxel positions

You can find position of taxel relative to world frame by taking known taxel coordinates
in local frame and applying `ROS TF2` transform from local to world frame.

An example of getting taxel position in world frame from a python script is in
`./gazebo9/catkin_ws/src/nao_explauto/python/end_effector_position.py`

<a name="changing"></a>
# Changing robot model description

I have copied robot model in URDF format from folder `./catkin_ws/src/nao_robot/nao_description/urdf/naoV40_generated_urdf` and performed the following changes:

- Removed fingers because `mimic_joint` was very buggy and interfered with the simulation
- Removed legs because I did not need them
- Removed cameras, bumper sensors, microphone etc. because I did not need them
- Fixed robot torso to the world frame
- Attached models for plastic casings to robot's head, torso and arms
- Added "pens" constructed from a cylinder and a sphere to robot arms for easier touch detection
- Generated artificial skin from list of coordinates and attached it to plastic casings
- Enabled `gazebo_contactsensor_plugin` on the artificial skin patches

These changes were saved to folders `naoSkin_generated_urdf` for low resolution and `naoSkin2_generated_urdf` for high resolution versions of the artificial skin.
