# Currently there are two versions of modified NAO:
# Skin  - lowres artificial skin simulation (default)
# Skin2 - highres artificial skin simulation
NAO_VERSION='Skin'

# Contact plugin loader accepts either true (default) or false
# Use this to disable contact plugin on body parts that are unused in particular
# experiment with high resolution skin configuration
# These can be set with -t, -h, -l, -r,- f
CONTACT_TORSO='true'
CONTACT_HEAD='true'
CONTACT_LEFT_ARM='true'
CONTACT_RIGHT_ARM='true'
FAKE_ARMS='false'

# Physics engine can be set with -p options
# Valid values are:
# ode - default physics engine
# bullet - this one was very buggy with contact detection
# dart - seems to work better than ode, stil not ideal
PHYSICS='ode'

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -v|--version)
    NAO_VERSION="$2"
    shift # past argument
    shift # past value
    ;;
    -p|--physics)
    PHYSICS="$2"
    shift # past argument
    shift # past value
    ;;
    -t|--torso)
    CONTACT_TORSO="$2"
    shift # past argument
    shift # past value
    ;;
    -h|--head)
    CONTACT_HEAD="$2"
    shift # past argument
    shift # past value
    ;;
    -l|--left_arm)
    CONTACT_LEFT_ARM="$2"
    shift # past argument
    shift # past value
    ;;
    -r|--right_arm)
    CONTACT_RIGHT_ARM="$2"
    shift # past argument
    shift # past value
    ;;
    -f|--fake_arms)
    FAKE_ARMS="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    shift # past argument
    ;;
esac
done

reset
roslaunch nao_gazebo_plugin nao_gazebo_skin.launch disable_controller:=false nao_version:=$NAO_VERSION contact_torso:=$CONTACT_TORSO contact_head:=$CONTACT_HEAD contact_left_arm:=$CONTACT_LEFT_ARM contact_right_arm:=$CONTACT_RIGHT_ARM physics:=$PHYSICS fake_arms:=$FAKE_ARMS

#rosrun rqt_ez_publisher rqt_ez_publisher --force-discover
#roslaunch nao_gazebo_plugin nao_gazebo.launch
