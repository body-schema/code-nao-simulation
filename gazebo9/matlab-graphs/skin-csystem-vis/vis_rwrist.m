clear all
close all
clc

% Create figure
figure
hold on

% Plot robot
scaleFactor = .1;
file_name = 'rwrist_casing.stl';
model = stlread(file_name);

stl = struct;
stl.vertices = model.Points  * scaleFactor;
stl.faces = model.ConnectivityList;

patch( ...
    stl, ...
    'FaceColor', [0.8 0.8 1.0], ...
	'EdgeColor', 'none', ...
	'FaceLighting', 'gouraud', ...
	'AmbientStrength', 0.15, ...
	'FaceAlpha', 1.0...
);

% Add a camera light, and tone down the specular highlighting

camlight('right');
material('dull');

% Fix the axes scaling, and set a nice view angle
axis('image');
view([135 35]);
%view([105 5]);



% Get skin coordinates
formatSpec = '%f';
skin_filename = './right-arm.txt';
fileID = fopen(skin_filename,'r');
skin = fscanf(fileID, formatSpec);
skin = reshape(skin, [7, numel(skin) / 7])';
fclose(fileID);



% Draw coordinate systems
normalScaleFactor = 0.01;
for i = 1:size(skin,1)
    x = skin(i, 1);
    y = skin(i, 2);
    z = skin(i, 3);
    n = skin(i, 4:6);
    
    roll = atan2(n(3), n(2)) + pi/2;
    yaw = atan2(n(1), -n(2)) + pi/2;
    pitch = -atan2(sqrt(n(1)^2 + n(2)^2), n(3));
    
    R = makehgtform('xrotate', roll, 'yrotate', pitch, 'zrotate', yaw);

    zaxis = n;
    xaxis = R(1:3,1);
    yaxis = cross(zaxis, xaxis);

    xaxis = xaxis * normalScaleFactor;
    yaxis = yaxis * normalScaleFactor;
    zaxis = zaxis * normalScaleFactor;
    
    plot3([x x+xaxis(1)], [y y+xaxis(2)], [z z+xaxis(3)], 'LineWidth', 2, 'Color', 'r');
    plot3([x x+yaxis(1)], [y y+yaxis(2)], [z z+yaxis(3)], 'LineWidth', 2, 'Color', 'g');
    plot3([x x+zaxis(1)], [y y+zaxis(2)], [z z+zaxis(3)], 'LineWidth', 2, 'Color', 'b');
end
