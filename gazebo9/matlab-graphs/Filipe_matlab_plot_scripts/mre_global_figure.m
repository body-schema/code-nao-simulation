function mre_global_figure(path)

if nargin < 1
  path = "../results_stats/";
end

%% Init
% List of set of exps folders
cd(path); % change dir
folders_struct = dir; % get list of set of exps folders
folders_names_cells = {folders_struct.name}; % get cell array of names
folders = string(folders_names_cells(3:end)); % into string array, ignore . and ..

% Plots parameters 
data_range = 100:100:1000; % test checkpoints (default: every 100 iterations)
co = get(gca,'ColorOrder'); % Initial colors
newColors = [co; 0 0 0; 1 0 0; 1 1 0; 1 0 1;]; % Add new colors for more functions on the same plot

% Misc
% index to start listing ind. exp folders. default: 0, if already ran the exps
% and some figures are created, should be +1 per figure created, depending on if
% they are added at the start or the end of the result of dir
DIR_IGNORE_START = 6;
DIR_IGNORE_END = 2;

%% Processing

% row = 1:6 lowres torso, 7:12 lowres head, 13:16 highres torso, 17:20 highres head
exp_mean_plots = zeros(size(folders, 2), size(data_range, 2));
fold_idx = 1;

% For each set of experiment
for fold = folders
  % Figure settings
  % have to be re-applied, otherwise it applies only on the first figure
  set(gca, 'FontSize', 29);
  set(gcf, 'Position', get(0, 'Screensize'));
  % This sets the new colour scheme and automatically opens a figure with it.
  set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
  
  % Figure labels and settings
  grid on
  xlabel('Iteration number')
  ylabel('Mean reaching distance [cm]')
  title('Comparison of same-exp results')
 
  cd(fold);
  exp_folds_struct = dir;  % get list of individual exps folders
  exp_names_cells = {exp_folds_struct.name}; % get cell array of names
  exps = string(exp_names_cells(3+DIR_IGNORE_START:end-DIR_IGNORE_END)); % into string array, ignore . and ..
  
  all_exps_mean = rand(size(exps, 2), size(data_range, 2));
  exp_idx = 1;
  
  % For each individual experiment, get the results and plot them
  for exp = exps
    mean_errs = zeros(size(data_range));
    datarange_idx = 1;
    stds = zeros(size(data_range));
    skin_portion = zeros(size(data_range));
    
    
    % For each test checkpoint (default: every 100 iterations)
    for i = data_range
      filename = strcat(exp, '/output/data-', string(i), '.txt');
      fileID = fopen(filename,'r');
      data = fscanf(fileID, '%f');
      data = reshape(data, [5, numel(data) / 5])';
      data_subset = data(find(data(:,5) ~= 9999), 5) .* 100;
      
      reaching_mean_err = mean(data_subset);
      reaching_std = std(data_subset);
      reaching_skin_portion = numel(data_subset) / numel(data(:,5));
      
      mean_errs(datarange_idx) = reaching_mean_err;
      stds(datarange_idx) = reaching_std;
      skin_portion(datarange_idx) = reaching_skin_portion;
      
      datarange_idx = datarange_idx + 1;
      fclose(fileID);
    end
    plot(data_range, mean_errs, '--', 'LineWidth', 3);
    hold on;
    all_exps_mean(exp_idx, 1:end) = mean_errs;
    exp_idx = exp_idx + 1;
  end
  final_means = nanmean(all_exps_mean);
  final_std = std(all_exps_mean);
  plot(data_range, final_means, 'LineWidth', 4);
  errorbar(data_range, final_means, final_std, 'Color', [1 0 1], 'LineWidth', 1.5);
  
  % Figure legend and settings
  %legend(labels, 'FontSize', 31, 'Location','NorthWest');
  axis([100 1000  0 15]);
  yticks([0:3:15]);
  xticks([100 300 500 800 1000]);
  
  % Save figure
  fig_name = strcat('MRE_', fold, '_cb');
  %saveas(gcf, fig_name, 'fig')
  %saveas(gcf, fig_name, 'epsc')
  
  exp_mean_plots(fold_idx, 1:end) = final_means;
  fold_idx = fold_idx + 1;
  
  cd ..;
  close all;
end

cd("../ICDL_newresults");
%% Plot final torso lowres
% Figure settings
set(gca, 'FontSize', 29);
set(gcf, 'Position', get(0, 'Screensize'));
set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
labels = ["RMB" "RGB" "DGB 15x15" "DGB 32x32" "RGB with DO" "DGB 32x32 with DO"];

plot(data_range, exp_mean_plots(1:6, 1:end), 'LineWidth', 3);
grid on
xlabel('Iteration number')
ylabel('Mean reaching distance [cm]')
%title('Comparison of exploration strategies, torso & right hand - Low res skin')
legend(labels, 'FontSize', 31, 'Location','NorthWest');
axis([100 1000 0 inf]);

fig_name = strcat('MRE_lowres_torso');
saveas(gcf, fig_name, 'fig')
saveas(gcf, fig_name, 'epsc')

%% Plot final head lowres
% Figure settings
set(gca, 'FontSize', 29);
set(gcf, 'Position', get(0, 'Screensize'));
set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
labels = ["RMB" "RGB" "DGB 15x15" "DGB 32x32" "RGB with DO" "DGB 32x32 with DO"];

plot(data_range, exp_mean_plots(7:12, 1:end), 'LineWidth', 3);
grid on
xlabel('Iteration number')
ylabel('Mean reaching distance [cm]')
%title('Comparison of exploration strategies, torso & right hand - Low res skin')
legend(labels, 'FontSize', 31, 'Location','NorthWest');
axis([100 1000  0 inf]);

fig_name = strcat('MRE_lowres_head');
saveas(gcf, fig_name, 'fig')
saveas(gcf, fig_name, 'epsc')

%% Plot final head lowres -- no head joints
% Figure settings
set(gca, 'FontSize', 29);
set(gcf, 'Position', get(0, 'Screensize'));
set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
labels = ["RMB" "RGB" "DGB 15x15" "DGB 32x32"];

plot(data_range, exp_mean_plots(13:16, 1:end), 'LineWidth', 3);
grid on
xlabel('Iteration number')
ylabel('Mean reaching distance [cm]')
%title('Comparison of exploration strategies, torso & right hand - Low res skin')
legend(labels, 'FontSize', 31, 'Location','NorthWest');
axis([100 1000  0 inf]);

fig_name = strcat('MRE_lowres_head_noJoints');
saveas(gcf, fig_name, 'fig')
saveas(gcf, fig_name, 'epsc')


%% Plot final torso highres
% Figure settings
set(gca, 'FontSize', 29);
set(gcf, 'Position', get(0, 'Screensize'));
set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
labels = ["RMB" "RGB" "DGB 15x15" "DGB 32x32"];

plot(data_range, exp_mean_plots(17:20, 1:end), 'LineWidth', 3);
grid on
xlabel('Iteration number')
ylabel('Mean reaching distance [cm]')
%title('Comparison of exploration strategies, torso & right hand - Low res skin')
legend(labels, 'FontSize', 31, 'Location','NorthWest');
axis([100 1000  0 inf]);

fig_name = strcat('MRE_highres_torso');
saveas(gcf, fig_name, 'fig')
saveas(gcf, fig_name, 'epsc')

%% Plot final head highres
% Figure settings
set(gca, 'FontSize', 29);
set(gcf, 'Position', get(0, 'Screensize'));
set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
labels = ["RMB" "RGB" "DGB 15x15" "DGB 32x32"];


plot(data_range, exp_mean_plots(21:24, 1:end), 'LineWidth', 3);
grid on
xlabel('Iteration')
ylabel('Mean reaching distance [cm]')
%title('Comparison of exploration strategies, torso & right hand - Low res skin')
legend(labels, 'FontSize', 31, 'Location','NorthWest');
axis([100 1000  0 inf]);

fig_name = strcat('MRE_highres_head');
saveas(gcf, fig_name, 'fig')
saveas(gcf, fig_name, 'epsc')

%% Plot final head highres -- noJoints
% Figure settings
set(gca, 'FontSize', 29);
set(gcf, 'Position', get(0, 'Screensize'));
set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
labels = ["RMB" "RGB" "DGB 15x15" "DGB 32x32"];


plot(data_range, exp_mean_plots(25:28, 1:end), 'LineWidth', 3);
grid on
xlabel('Iteration')
ylabel('Mean reaching distance [cm]')
%title('Comparison of exploration strategies, torso & right hand - Low res skin')
legend(labels, 'FontSize', 31, 'Location','NorthWest');
axis([100 1000  0 inf]);

fig_name = strcat('MRE_highres_head_noJoints');
saveas(gcf, fig_name, 'fig')
saveas(gcf, fig_name, 'epsc')

%% end, cleaning and reseting path
% For convenience, go back to the scripts folder
close all;
cd '../mat_scripts';
clear;
end