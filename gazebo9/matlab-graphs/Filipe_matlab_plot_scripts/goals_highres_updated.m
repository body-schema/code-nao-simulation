clc;
clear all;
close all;

set(gca, 'FontSize', 26);
set(gcf, 'Position', get(0, 'Screensize'));

ay=gca;
ay.YAxis.TickLabelFormat = '%.2f';
ay.XAxis.TickLabelFormat = '%.2f';

% Create colormap
colorMap = jet(256);

% Get skin coordinates
formatSpec = '%f';
skin_filename = '../coordinate-maps/highres-head.txt';
fileID = fopen(skin_filename,'r');
skin = fscanf(fileID, formatSpec);
skin = reshape(skin, [2, numel(skin) / 2])';
fclose(fileID);

% Get goals
formatSpec = '%f';
goal_filename = '../datasets/exp053/output/goals.txt';
fileID = fopen(goal_filename,'r');
goalss = fscanf(fileID, formatSpec);
goalss = reshape(goalss, [2, numel(goalss) / 2])';
fclose(fileID);

% get reaching error distance
filename = '../datasets/exp053/output/data-1000.txt';
fileID = fopen(filename,'r');
errors = fscanf(fileID, formatSpec);
errors = reshape(errors, [5, numel(errors) / 5])';

hold on
grid on
i = 0;
none_index = [];
h = zeros(5, 1);
h(1) = scatter(goalss(:,1),goalss(:,2), [], [.7,.7,.7], 'filled');
h(2) = scatter(skin(:,1),skin(:,2), 'black', 'filled');
for i = 1:size(errors, 1)
  if errors(i, 5) == 0.0
    h(7) = viscircles([errors(i, 1) errors(i, 2)], 0.004, 'Color', 'blue');
  elseif errors(i, 5) < 9999
    h(6) = viscircles([errors(i, 1) errors(i, 2)],abs(errors(i, 5)), 'Color', 'magenta');
  else
    h(7) = viscircles([errors(i, 1) errors(i, 2)], 0.004, 'Color', 'red');
  end
end
for i = 1:size(errors, 1)
  if errors(i, 5) == 0.0
    h(3) = scatter(errors(i,1), errors(i, 2), 'blue', 'filled');
  elseif errors(i, 5) < 9999
    h(5) = scatter(errors(i,1), errors(i, 2), 'magenta', 'filled');
  else
    h(4) = scatter(errors(i,1), errors(i,2), 'red', 'filled');
  end
end
legend(h, 'Goals', 'Untested taxels', 'Reached taxels', 'Unreached', 'Reached with error','FontSize', 33, 'Location','NorthEast');
%legend(h, 'Goals','Artificial skin taxels', 'Mean Reaching Error / 5');
%title('Distribution of goals in Direct Optimization Discrete Goal Babbling, 32x32')

x_min = min(skin(:,1));
x_max = max(skin(:,1));
y_min = min(skin(:,2));
y_max = max(skin(:,2));
dx = x_max - x_min;
dy = y_max - y_min;

%xlim([x_min-dx x_max+dx]);
ylim([y_min-dy y_max+dy]);

ylim([-0.20 0.60])

%xticks([x_min-dx x_min x_max x_max+dx]);
%yticks([y_min-dy y_min y_max y_max+dy]);

% xticks([x_min-dx:0.1:x_max+dx]);
% yticks([y_min-dy:0.05:y_max+dy]);

%axis equal
% Random GB
saveas(gcf,'RandomGoalBabbling_cb10.fig')
saveas(gcf,'RandomGoalBabbling_cb10', 'epsc')
% Discrete GB 15
% saveas(gcf,'DiscretisedGoalBabbling15_cb10.fig')
% saveas(gcf,'DiscretisedGoalBabbling15_cb10', 'epsc')
% % Discrete GB 32
% saveas(gcf,'DiscretisedGoalBabbling32_cb10.fig')
% saveas(gcf,'DiscretisedGoalBabbling32_cb10', 'epsc')
% % Direct Opti RGB
% saveas(gcf,'DirectOptimizedRandomGoalBabbling.fig')
% saveas(gcf,'DirectOptimizedRandomGoalBabbling', 'epsc')
% % Tree (Sagg-riac)
% saveas(gcf,'Tree.fig')
% saveas(gcf,'Tree', 'epsc')
% % Direct Opti DGB 32
% saveas(gcf,'DirectOptimizedDiscretisedGB32_cb10.fig')
% saveas(gcf,'DirectOptimizedDiscretisedGB32_cb10', 'epsc')
