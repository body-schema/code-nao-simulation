epstopdf DiscretisedGoalBabbling15_cb10.eps
epstopdf DiscretisedGoalBabbling32_cb10.eps
epstopdf RandomGoalBabbling_cb10.eps

pdfcrop DiscretisedGoalBabbling15_cb10.pdf
pdfcrop DiscretisedGoalBabbling32_cb10.pdf
pdfcrop RandomGoalBabbling_cb10.pdf

rm DiscretisedGoalBabbling15_cb10.pdf
rm DiscretisedGoalBabbling32_cb10.pdf
rm RandomGoalBabbling_cb10.pdf

mv DiscretisedGoalBabbling15_cb10-crop.pdf DiscretisedGoalBabbling15_cb10.pdf
mv DiscretisedGoalBabbling32_cb10-crop.pdf DiscretisedGoalBabbling32_cb10.pdf
mv RandomGoalBabbling_cb10-crop.pdf RandomGoalBabbling_cb10.pdf
