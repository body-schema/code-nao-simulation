function goal_or_home(path, transitions, classes)

cd(path);
transitions = cell2mat(transitions(1));
classes = classes{1};

actual_transitions = transpose(find(transitions == 1));

% transitions counts
continued_next_goal = 0; % from an endgoal to another endgoal
go_back_home = 0; % from an endgoal to home
go_back_goal = 0; %from home to goal should be equal to go_back_home +/- 1
should_not_happen = 0; %another case
stay_home = 0; % from home to home - shouldn't happen
total_towards_goal = 0;

% step counts
average_toGoal_steps = 0.0;
average_goHome_steps = 0.0;
last_i = 1;
nb_steps_go_goal = 0;
nb_steps_go_home = 0;
nb_steps_lost_in_oblivion = 0;

for i = actual_transitions
  
  trans = classes{i};
  trans_next = classes{i+1};
  if (strcmp(trans, "StateGoalPath")) && (strcmp(trans_next, "StateGoHomePosture"))
    go_back_home = go_back_home + 1;
    nb_steps_go_goal = nb_steps_go_goal + (i - last_i);
  elseif (strcmp(trans, "StateGoalPath")) && (strcmp(trans_next, "StateGoalPath"))
    continued_next_goal = continued_next_goal + 1;
    nb_steps_go_goal = nb_steps_go_goal + (i - last_i);
  elseif (strcmp(trans, "StateGoHomePosture")) && (strcmp(trans_next, "StateGoHomePosture"))
    stay_home = stay_home + 1;
    nb_steps_go_home = nb_steps_go_home + (i - last_i);
  elseif (strcmp(trans, "StateGoHomePosture")) && (strcmp(trans_next, "StateGoalPath"))
    go_back_goal = go_back_goal + 1;
    nb_steps_go_home = nb_steps_go_home + (i - last_i);
  else
    should_not_happen = should_not_happen + 1;
    nb_steps_lost_in_oblivion = nb_steps_lost_in_oblivion + (i - last_i);
  end
  last_i = i;
end
total_towards_goal = continued_next_goal + go_back_goal;
average_toGoal_steps = nb_steps_go_goal / total_towards_goal;
average_goHome_steps = nb_steps_go_home / (go_back_home + stay_home);
clear all;
close all;
end