function projections_goals_stats_by100step(path, skin_id)

if nargin < 2
  skin_id = 1;
  if nargin < 1
    path = "../results_stats/";
  end
end

%% Init
data_range = 100:100:1000; % test checkpoints (default: every 100 iterations)
script_path = pwd; % save script path, to call it later
cd(path); % change dir to the one containing the experiments

% lowres torso
if skin_id == 1
  folders = ["exp001", "exp003", "exp004", "exp005", "exp006", "exp008"];
  skin_filename = '../coordinate-maps/lowres-torso.txt';
  % Edges for better readability of the plot
  edges = [
    1 5; 1 6; 2 6; 2 7; 3 7; 3 8; 4 8; 4 9;
    5 10; 6 11; 7 12; 8 13; 9 14;
    10 15; 11 15; 11 16; 12 16; 12 17; 13 17; 13 18; 14 18;
    15 19; 16 20; 17 21; 18 22;
    19 23; 20 23; 20 24;21 24; 21 25; 22 25
  ];
% lowres head
elseif skin_id == 2
  folders = ["exp011", "exp013", "exp014", "exp015", "exp016", "exp018"];
  skin_filename = '../coordinate-maps/lowres-head.txt';
  % Edges for better readability of the plot
  edges = [
    1 2; 1 7; 2 3; 3 4; 3 9; 4 5; 5 11;
    6 7; 6 18; 7 8; 8 9; 9 10; 10 11; 11 12; 12 24;
    13 14; 13 19; 14 15; 15 16; 15 21; 16 17; 17 23;
    18 19; 19 20; 20 21; 21 22; 22 23; 23 24;
  ];
% lowres head no head joints
elseif skin_id == 3
  folders = ["exp021", "exp023", "exp024", "exp025"];
  skin_filename = '../coordinate-maps/lowres-head.txt';
  % Edges for better readability of the plot
  edges = [
    1 2; 1 7; 2 3; 3 4; 3 9; 4 5; 5 11;
    6 7; 6 18; 7 8; 8 9; 9 10; 10 11; 11 12; 12 24;
    13 14; 13 19; 14 15; 15 16; 15 21; 16 17; 17 23;
    18 19; 19 20; 20 21; 21 22; 22 23; 23 24;
  ];
% highres torso
elseif skin_id == 4
  folders = ["exp041", "exp043", "exp044", "exp045"];
  skin_filename = '../coordinate-maps/highres-torso.txt';
  edges = [];
% highres head
elseif skin_id == 5
  folders = ["exp051", "exp053", "exp054", "exp055"];
  skin_filename = '../coordinate-maps/highres-head.txt';
  edges = [];
% highres head no head joints
else % if skin_id == 6, in particular, but covers other errors/mistype/abuse
  folders = ["exp061", "exp063", "exp064", "exp065"];
  skin_filename = '../coordinate-maps/highres-head.txt';
  edges = [];
end

% Plots parameters 
colorMap = jet(256); % Create colormap

% Constants
% index to start listing ind. exp folders. default: 0, if already ran the exps
% and some figures are created, should be +1 per figure created, depending on if
% they are added at the start or the end of the result of dir
DIR_IGNORE_START = 6;
DIR_IGNORE_END = 2;
ERROR_SCALING = 5; % Divides the reaching error by this value

% Get skin coordinates
fileID = fopen(skin_filename,'r');
skin = fscanf(fileID, '%f');
skin = reshape(skin, [2, numel(skin) / 2])';
fclose(fileID);

% Prepare axis limits from skin coordinates values
x_min = min(skin(:,1));
x_max = max(skin(:,1));
y_min = min(skin(:,2));
y_max = max(skin(:,2));
dx = x_max - x_min;
dy = y_max - y_min;


%% Processing
% For each set of experiment
for fold = folders
  cd(fold);
  exp_folds_struct = dir;  % get list of individual exps folders
  exp_names_cells = {exp_folds_struct.name}; % get cell array of names
  exps = string(exp_names_cells(3+DIR_IGNORE_START:end-DIR_IGNORE_END)); % into string array, ignore . and ..
  
  % For each individual experiment, get the results and plot them
  for exp = exps
    cd(exp);
    
    % Check if goals exists, if yes, load them
    if exist('./output/goals.txt', 'file') == 2
      % Get goals
      goals_filename = './output/goals.txt';
      fileID = fopen(goals_filename, 'r');
      goals = fscanf(fileID, '%f');
      goals = reshape(goals, [2, numel(goals) / 2])';
      fclose(fileID);
      
      idx_endstep_goal = size(goals, 1) - 900;
    end
    
    % For each test checkpoint (default: every 100 iterations)
    for t_step = data_range
      cd("output/");
      
      % Init/reset variables
      % control variables, set to 1 when the corresponding item has been plotted once
      % Goals, Skin taxels, reached_taxels, unreached_taxels, reached_with_errors
      ctrl_handles = [0, 0, 0, 0, 0];
      handles = zeros(5); % Contains plot handles
      
      % Figure settings
      % have to be re-applied, otherwise it applies only on the first figure
      set(gca, 'FontSize', 26);
      set(gcf, 'Position', get(0, 'Screensize'));
      ay = gca;
      ay.YAxis.TickLabelFormat = '%.2f';
      ay.XAxis.TickLabelFormat = '%.2f';
      grid on;
      hold on;
      
      % Check if goals exists, if yes, plot them
      if exist('goals.txt', 'file') == 2
        % Plot while keeping handle + set flag
        handles(1) = scatter(goals(1:idx_endstep_goal, 1), goals(1:idx_endstep_goal, 2), [], [.7,.7,.7], 'filled');
        ctrl_handles(1) = 1;
        idx_endstep_goal = idx_endstep_goal + 100; % increment for next step
      end
      
      % Check if it's high-res skin, in which case draw all skin taxels
      if contains(skin_filename, "highres")
        % We already have the data in the skin variable, we just plot them with
        % the handle and setting the flag
        handles(2) = scatter(skin(:,1),skin(:,2), 'black', 'filled');
        ctrl_handles(2) = 1;
      end
      
      % Draw edges, if there are any to be drawn
      if ~isempty(edges)
        for i = 1:size(edges,1)
          plot([skin(edges(i,1),1) skin(edges(i,2),1)], [skin(edges(i,1),2) skin(edges(i,2),2)], 'Color', [.7,.7,1]);
        end
      end
      
      % Get reaching errors
      filename = strcat('data-', string(t_step), '.txt');
      %filename = './output/data-1000.txt';
      fileID = fopen(filename,'r');
      errors = fscanf(fileID, '%f');
      errors = reshape(errors, [5, numel(errors) / 5])';
      fclose(fileID);
      for i = 1:size(errors, 1)
        if errors(i, 5) == 0.0
          viscircles([errors(i, 1) errors(i, 2)], 0.003, 'Color', 'blue');
        elseif errors(i, 5) < 9999
          viscircles([errors(i, 1) errors(i, 2)],abs(errors(i, 5)/ERROR_SCALING), 'Color', 'magenta');
        else
          viscircles([errors(i, 1) errors(i, 2)], 0.003, 'Color', 'red');
        end
      end
      % Plot the dots - has to be done after circles are finished being drawn, as
      % otherwise they can overlap and hide the taxels dots
      for i = 1:size(errors, 1)
        if errors(i, 5) == 0.0
          handles(3) = scatter(errors(i,1), errors(i, 2), 'blue', 'filled');
          ctrl_handles(3) = 1;
        elseif errors(i, 5) < 9999
          handles(5) = scatter(errors(i,1), errors(i, 2), 'magenta', 'filled');
          ctrl_handles(5) = 1;
        else
          handles(4) = scatter(errors(i,1), errors(i,2), 'red', 'filled');
          ctrl_handles(4) = 1;
        end
      end
      
      % Set legend
      leg_idx = 1;

     % if we don't use (1,nnz), will create (nnz x nnz) matrix
      legends = strings(1, nnz(ctrl_handles)); % number of non-zero elements
      if ctrl_handles(1) == 1
        legends(leg_idx) = "Goals";
        leg_idx = leg_idx + 1;
      end
      if ctrl_handles(2) == 1
        legends(leg_idx) = "Untested taxels";
        leg_idx = leg_idx + 1;
      end
      if ctrl_handles(3) == 1
        legends(leg_idx) = "Reached taxels";
        leg_idx = leg_idx + 1;
      end
      if ctrl_handles(4) == 1
        legends(leg_idx) = "Unreached";
        leg_idx = leg_idx + 1;
      end
      if ctrl_handles(5) == 1
        if ERROR_SCALING ~= 1
          leg_err_scaled = strcat("Reached with error", '/', num2str(ERROR_SCALING));
          legends(leg_idx) = leg_err_scaled;
        else
          legends(leg_idx) = "Reached with error";
        end
      end
      handles(handles==0) = [];
      legend(handles, legends, 'FontSize', 33, 'Location','NorthEast');
      
       % Axis settings
      if contains(skin_filename, "highres-head")
        xlim([-0.30 0.30])
        ylim([-0.20 0.60])
      else
        xlim([x_min-dx x_max+dx]);
        ylim([y_min-dy y_max+dy]);
        axis equal
      end
      
      % Save figure
      fig_name = strcat('data-',  string(t_step), '_Proj_', exp, '_cb');
      saveas(gcf, fig_name, 'fig')
      saveas(gcf, fig_name, 'epsc')
      
      cd ..;
      close all;
    end
    
    cd ..;
  end

  cd ..;
end

cd(script_path);
% if skin_id > 0 && skin_id < 6
%   projections_goals_stats_by100step(path, skin_id+1);
% end
clear;
end