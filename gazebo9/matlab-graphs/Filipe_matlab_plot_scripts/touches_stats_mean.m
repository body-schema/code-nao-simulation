function touches_stats(path)

if nargin < 1
  path = "../results_stats/";
end

%% Init
% List of set of exps folders
cd(path); % change dir
folders_struct = dir; % get list of set of exps folders
folders_names_cells = {folders_struct.name}; % get cell array of names
folders = string(folders_names_cells(3:end)); % into string array, ignore . and ..

% Plots parameters 
data_range = 100:100:1000; % test checkpoints (default: every 100 iterations)
co = get(gca,'ColorOrder'); % Initial colors
newColors = [co; 0 0 0; 1 0 0; 1 1 0; 1 0 1;]; % Add new colors for more functions on the same plot

% Misc
% index to start listing ind. exp folders. default: 0, if already ran the exps
% and some figures are created, should be +1 per figure created, depending on if
% they are added at the start or the end of the result of dir
DIR_IGNORE_START = 2;
DIR_IGNORE_END = 0;


%% Processing
% For each set of experiment
for fold = folders
  % Figure settings
  % have to be re-applied, otherwise it applies only on the first figure
  set(gca, 'FontSize', 29);
  set(gcf, 'Position', get(0, 'Screensize'));
  % This sets the new colour scheme and automatically opens a figure with it.
  set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
  
  % Figure labels and settings
  grid on
  xlabel('Iteration number')
  ylabel('Number of touches')
  title('Comparison of same-exp results')
 
  cd(fold);
  exp_folds_struct = dir;  % get list of individual exps folders
  exp_names_cells = {exp_folds_struct.name}; % get cell array of names
  exps = string(exp_names_cells(3+DIR_IGNORE_START:end-DIR_IGNORE_END)); % into string array, ignore . and ..
  
  all_touches = zeros(size(exps, 2), size(data_range, 2));
  exp_idx = 1;
  
  % For each individual experiment, get the results and plot them
  for exp = exps
    filename = strcat(exp, '/output/touches.txt');
    fileID = fopen(filename,'r');
    data = fscanf(fileID, '%i');
    
    plot(data_range, data, '--', 'LineWidth', 3);
    hold on;
    
    all_touches(exp_idx, 1:end) = transpose(data);
    exp_idx = exp_idx + 1;
    
    fclose(fileID);
  end
  
  touches_mean = mean(all_touches);
  final_std = std(all_touches);
  plot(data_range, touches_mean, 'LineWidth', 4);
  errorbar(data_range, touches_mean, final_std, 'Color', [1 0 1], 'LineWidth', 1.5);
  
  % Figure legend and settings
  %legend(labels, 'FontSize', 31, 'Location','NorthWest')
  axis([100 1000  0 1000]);
  yticks([100 300 500 650 800 1000]);
  xticks([100 300 500 800 1000]);
  
  % Save figure
  fig_name = strcat('nbTouches_', fold, '_cb');
  saveas(gcf, fig_name, 'fig')
  saveas(gcf, fig_name, 'epsc')
  
  cd ..;
  close all;
end
% For convenience, go back to the scripts folder
cd '../mat_scripts';
clear;
end