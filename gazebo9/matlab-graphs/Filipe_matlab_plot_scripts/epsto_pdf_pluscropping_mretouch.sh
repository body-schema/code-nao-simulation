epstopdf MeanRD_fullcb10.eps
epstopdf MeanRD_NoSagg-NoConstrained_cb10.eps
epstopdf nbTouches_Full.eps
epstopdf nbTouches_NoSagg-NoConstrained_cb10.eps

pdfcrop MeanRD_fullcb10.pdf
pdfcrop MeanRD_NoSagg-NoConstrained_cb10.pdf
pdfcrop nbTouches_Full.pdf
pdfcrop nbTouches_NoSagg-NoConstrained_cb10.pdf

rm MeanRD_fullcb10.pdf
rm MeanRD_NoSagg-NoConstrained_cb10.pdf
rm nbTouches_Full.pdf
rm nbTouches_NoSagg-NoConstrained_cb10.pdf

mv MeanRD_fullcb10-crop.pdf MeanRD_fullcb10.pdf
mv MeanRD_NoSagg-NoConstrained_cb10-crop.pdf MeanRD_NoSagg-NoConstrained_cb10.pdf
mv nbTouches_Full-crop.pdf nbTouches_Full.pdf
mv nbTouches_NoSagg-NoConstrained_cb10-crop.pdf nbTouches_NoSagg-NoConstrained_cb10.pdf
