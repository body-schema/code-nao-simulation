fclose all
clear all
close all
clc
set(gca, 'FontSize', 29);
set(gcf, 'Position', get(0, 'Screensize'));

% For Low resolution Right hand - torso
compare_dirs = ["../datasets/exp001/" "../datasets/exp002/" "../datasets/exp003cb10/" "../datasets/exp004cb10/" "../datasets/exp005cb10/" "../datasets/exp006/" "../datasets/exp007/" "../datasets/exp008cb10/"];
labels = ["RMB" "Constrained RMB" "RGB" "DGB 15x15" "DGB 32x32" "RGB with DO" "Tree (SAGG-RIAC)" "DGB 32x32 with DO"];

% For Low resolution Right hand - torso / without sagg riacc and constrained motor babbling
compare_dirs = ["../datasets/exp001/" "../datasets/exp003cb10/" "../datasets/exp004cb10/" "../datasets/exp005cb10/" "../datasets/exp006/" "../datasets/exp008cb10/"];
labels = ["RMB" "RGB" "DGB 15x15" "DGB 32x32" "RGB with DO" "DGB 32x32 with DO"];

% For Low resolution Right hand - head
compare_dirs = ["../datasets/exp011/" "../datasets/exp012/" "../datasets/exp013cb3_2/" "../datasets/exp014/" "../datasets/exp015/" "../datasets/exp016_2/" "../datasets/exp017/" "../datasets/exp018cb10/"];
labels = ["RMB" "Constrained RMB" "RGB" "DGB 15x15" "DGB 32x32" "RGB with DO" "Tree (SAGG-RIAC)" "DGB 32x32 with DO"];

% For Low resolution Right hand - head / without sagg riacc and constrained motor babbling
compare_dirs = ["../datasets/exp011/" "../datasets/exp013cb3_2/" "../datasets/exp014/" "../datasets/exp015/" "../datasets/exp016_2/" "../datasets/exp018cb10/"];
labels = ["RMB" "RGB" "DGB 15x15" "DGB 32x32" "RGB with DO" "DGB 32x32 with DO"];

% For High resolution Right hand - torso
compare_dirs = ["../datasets/exp041/" "../datasets/exp042/" "../datasets/exp043cb10/" "../datasets/exp044cb10/" "../datasets/exp045cb10delay3/" "../datasets/exp047cb10/"];
labels = ["RMB" "Constrained RMB" "RGB" "DGB 15x15" "DGB 32x32" "Tree (SAGG-RIAC)"];

% For High resolution Right hand - torso / without sagg riacc and constrained motor babbling
compare_dirs = ["../datasets/exp041/" "../datasets/exp043cb10/" "../datasets/exp044cb10/" "../datasets/exp045cb10delay3/"];
labels = ["RMB" "RGB" "DGB 15x15" "DGB 32x32"];

% For High resolution Right hand - head
compare_dirs = ["../datasets/exp051/" "../datasets/exp052/" "../datasets/exp053/" "../datasets/exp054/" "../datasets/exp055/" "../datasets/exp057/"];
labels = ["RMB" "Constrained RMB" "RGB" "DGB 15x15" "DGB 32x32" "Tree (SAGG-RIAC)"];

% For High resolution Right hand - head / without sagg riacc and constrained motor babbling
compare_dirs = ["../datasets/exp051/" "../datasets/exp053/" "../datasets/exp054/" "../datasets/exp055/"];
labels = ["RMB" "RGB" "DGB 15x15" "DGB 32x32"];

% compare_dirs = ["../datasets/exp011/" "../datasets/exp012/" "../datasets/exp013/" "../datasets/exp014/" "../datasets/exp015/" "../datasets/exp016/" "../datasets/exp017/"];
% labels = ["Random Motor Babbling" "Constrained Random Motor Babbling" "Random Goal Babbling" "Discretized Goal Babbling, 15x15 grid" "Discretized Goal Babbling, 32x32 grid" "Direct Optimization Goal Babbling" "Tree (SAGG-RIAC), 10 points per region"];

% compare_dirs = ["../datasets/exp003/" "../datasets/exp003cb2/" "../datasets/exp003cb3/" "../datasets/exp003cb4/" "../datasets/exp003cb10/" "../datasets/exp003cb20/"];
% labels = ["RGB default (23)" "RGB 2 touches" "RGB 3 touches" "RGB 4 touches" "RGB 10 touches" "RGB 20 touches"];

% compare_dirs = ["../datasets/exp003/" "../datasets/exp003cb3/" "../datasets/exp004/" "../datasets/exp004cb3/" "../datasets/exp005/" "../datasets/exp005cb3/" "../datasets/exp006/" "../datasets/exp007/" "../datasets/exp007cb3/"];
% labels = ["RGB default (23)" "RGB 3 touches" "Discrete 15x15 default(28)" "Discrete 15x15 3 touches" "Discrete 32x32 default(25)" "Discrete 32x32 3 touches" "Discrete Optimization 3 touches" "Tree default (32)" "Tree 3 touches"];

% compare_dirs = ["../datasets/exp005/" "../datasets/exp005cb3/" "../datasets/exp005cb5/" "../datasets/exp005cb10/" "../datasets/exp005cb20/"];
% labels = ["DGB default (~25)" "DGB 3 touches" "DGB 5 touches" "DGB 10 touches" "DGB 20 touches"];

% compare_dirs = ["../datasets/exp045cb10delay2/" "../datasets/exp045cb10delay3/" "../datasets/exp045cb10delay4/" "../datasets/exp045cb10delay5/"];
% labels = ["DGB 32 - delay 2s" "DGB 32 - delay 3s" "DGB 32 - delay 4s" "DGB 32 - delay 5s"];

data_range = 100:100:1000;
% data_range = 10:10:500;
formatSpec = '%f';


co = get(gca,'ColorOrder'); % Initial
newColors = [co; 0 0 0;];
% Change to new colors.
set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
% This automatically opens a figure with the new colour scheme
co = get(gca,'ColorOrder'); % Verify it changed

grid on
xlabel('Iteration number')
ylabel('Mean reaching distance [cm]')
%title('Comparison of exploration strategies, torso & right hand - High res skin')

for dir = compare_dirs
    means = [];
    stds = [];
    skin_portion = [];
    
    for i = data_range
        filename = strcat(dir, 'output/data-', string(i), '.txt');
        fileID = fopen(filename,'r');
        data = fscanf(fileID, formatSpec);
        data = reshape(data, [5, numel(data) / 5])';
        data_subset = data(find(data(:,5) ~= 9999), 5) .* 100;
        
        [i numel(data_subset)];
        
        reaching_mean = mean(data_subset);
        reaching_std = std(data_subset);
        reaching_skin_portion = numel(data_subset) / numel(data(:,5));
        
        means = [means, reaching_mean];
        stds = [stds, reaching_std];
        skin_portion = [skin_portion, reaching_skin_portion];
    end

    %errorbar(data_range, means, stds, 'LineWidth', 1, 'CapSize', 6);
    plot(data_range, means, 'LineWidth', 3);
    hold on;
end

legend(labels, 'FontSize', 31, 'Location','NorthWest');
axis([10 inf 0 inf]);

yticks([0:2:15]);

% saveas(gcf,'MeanRD_fullcb10.fig')
% saveas(gcf,'MeanRD_fullcb10', 'epsc')
saveas(gcf,'MeanRD_NoSagg-NoConstrained_cb10.fig')
saveas(gcf,'MeanRD_NoSagg-NoConstrained_cb10', 'epsc')

fclose all;
