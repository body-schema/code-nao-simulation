newColors = [co; 0 0 0; 1 0 0; 1 1 0;]; % Add new colors for more functions on the same plot
set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
grid on
plot(sin(0:0.1:10))
hold on
plot(sin(0:0.1:9))
plot(sin(0:0.1:8))
plot(sin(0:0.1:7))
plot(sin(0:0.1:6))
plot(sin(0:0.1:5))
plot(sin(0:0.1:4))
plot(sin(0:0.1:3))
plot(sin(0:0.1:2))
plot(sin(0:0.1:1))