function projections_3D_matthias_alg(path, skin_id)

if nargin < 2
  skin_id = 4;
  if nargin < 1
    path = "../results_stats/";
  end
end

%% Init
data_range1 = 100:100:1000; % test checkpoints (default: every 100 iterations)
data_range2 = 1000:1000:25000;
script_path = pwd; % save script path, to call it later
cd(path); % change dir to the one containing the experiments

% lowres torso
if skin_id == 1
  folder = "exp0011";
  skin_filename = '../coordinate-maps/lowres-torso.txt';
elseif skin_id == 2
  folder = "exp0111";
  skin_filename = '../coordinate-maps/lowres-head.txt';
% lowres head no head joints
elseif skin_id == 3
  folder = "exp0211";
  skin_filename = '../coordinate-maps/lowres-head.txt';
% highres torso
elseif skin_id == 4
  folder = "exp0411";
  skin_filename = '../coordinate-maps/highres-torso.txt';
% highres head
elseif skin_id == 5
  folder = "exp0511";
  skin_filename = '../coordinate-maps/highres-head.txt';
% highres head no head joints
else % if skin_id == 6, in particular, but covers other errors/mistype/abuse
  folder = "exp0611";
  skin_filename = '../coordinate-maps/highres-head.txt';
end

% Plots parameters 
colorMap = jet(256); % Create colormap

% Constants
% index to start listing ind. exp folders. default: 0, if already ran the exps
% and some figures are created, should be +1 per figure created, depending on if
% they are added at the start or the end of the result of dir
DIR_IGNORE_START = 0;
DIR_IGNORE_END = 1;
ERROR_SCALING = 5; % Divides the reaching error by this value

% Get skin coordinates
file_skin = fopen(skin_filename,'r');
skin = fscanf(file_skin, '%f');
skin = reshape(skin, [2, numel(skin) / 2])';
fclose(file_skin);

% Prepare axis limits from skin coordinates values
x_min = min(skin(:,1));
x_max = max(skin(:,1));
y_min = min(skin(:,2));
y_max = max(skin(:,2));
dx = x_max - x_min;
dy = y_max - y_min;

% Get skin coordinates
fileID = fopen(skin_filename,'r');
skin = fscanf(fileID, '%f');
skin = reshape(skin, [2, numel(skin) / 2])';
fclose(fileID);

% For set of experiment
cd(folder);
exp_folds_struct = dir;  % get list of individual exps folders
exp_names_cells = {exp_folds_struct.name}; % get cell array of names
exps = string(exp_names_cells(3+DIR_IGNORE_START:end-DIR_IGNORE_END)); % into string array, ignore . and ..

% For each individual experiment, get the results and plot them
for exp = exps
  cd(exp);
  % Get all states
  %filename = strcat('data-', string(t_step), '.txt');
  filename = './output/states.txt';
  file_states = fopen(filename,'r');
  states = textscan(file_states,'%d %f %f %f %f %f %f %f %f %f %f %d %s');
  %states = fscanf(file_states, '%s');
  fclose(file_states);
  
  full_path = pwd;
  %number of times we continue to another end goal or go back home
  %cd(script_path);
  %goal_or_home(full_path, states(12), states(13));
  
  %Plot the weights associated with touch outcomes, and analyse range of values
  %cd(script_path);
  %weight_ranges(full_path, states(9), states(7));
  
  %TODO: Plot the go to goal interm goals + end goal + go back home trajectories
  %for each back and forth
  cd(script_path);
  plot_goal_steps(full_path, states, skin, skin_filename);
  
  %TODO: Plot the differences between target and actual motor configuration
  %cd(script_path);
  %motor_discrepancy(full_path, skin_filename);
  
  cd(full_path);
  cd ..;
end


cd(script_path);
% if skin_id > 0 && skin_id < 6
%   projections_3D_matthias_alg(path, skin_id+1);
% end
clear;
end
