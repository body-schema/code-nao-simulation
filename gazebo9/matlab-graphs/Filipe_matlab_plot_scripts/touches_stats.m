function touches_stats(path)

if nargin < 1
  path = "../results_stats/";
end

%% Init
% List of set of exps folders
cd(path); % change dir
folders_struct = dir; % get list of set of exps folders
folders_names_cells = {folders_struct.name}; % get cell array of names
folders = string(folders_names_cells(3:end)); % into string array, ignore . and ..

% Plots parameters 
data_range = 100:100:1000; % test checkpoints (default: every 100 iterations)
co = get(gca,'ColorOrder'); % Initial colors
newColors = [co; 0 0 0; 1 0 0; 1 1 0;]; % Add new colors for more functions on the same plot

% Misc
% index to start listing ind. exp folders. default: 0, if already ran the exps
% and some figures are created, should be +1 per figure created, depending on if
% they are added at the start or the end of the result of dir
DIR_IGNORE_START = 2;
DIR_IGNORE_END = 2;


%% Processing
% For each set of experiment
for fold = folders
  % Figure settings
  % have to be re-applied, otherwise it applies only on the first figure
  set(gca, 'FontSize', 29);
  set(gcf, 'Position', get(0, 'Screensize'));
  % This sets the new colour scheme and automatically opens a figure with it.
  set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
  
  % Figure labels and settings
  grid on
  xlabel('Iteration number')
  ylabel('Number of touches')
  title('Comparison of same-exp results')
 
  cd(fold);
  exp_folds_struct = dir;  % get list of individual exps folders
  exp_names_cells = {exp_folds_struct.name}; % get cell array of names
  exps = string(exp_names_cells(3+DIR_IGNORE_START:end-DIR_IGNORE_END)); % into string array, ignore . and ..
  
  % For each individual experiment, get the results and plot them
  for exp = exps
    means = [];
    stds = [];
    
    filename = strcat(exp, '/output/touches.txt');
    fileID = fopen(filename,'r');
    data = fscanf(fileID, '%i');
    
    reaching_mean = mean(data);
    reaching_std = std(data);
    means = [means, reaching_mean];
    stds = [stds, reaching_std];
    
    fclose(fileID);
    plot(data_range, data, 'LineWidth', 3);
    hold on;
  end
  
  % Figure legend and settings
  %legend(labels, 'FontSize', 31, 'Location','NorthWest')
  
  % Save figure
  fig_name = strcat('nbTouches_', fold, '_cb');
  %saveas(gcf, fig_name, 'fig')
  %saveas(gcf, fig_name, 'epsc')
  
  cd ..;
  close all;
end
% For convenience, go back to the scripts folder
cd '../mat_scripts';
clear;
end