function motor_discrepancy(path, skin_filename)

nb_joints = 5;
if contains(skin_filename, "head")
  nb_joints = 7;
end

cd(path);
filename = './output/motors.txt';
file_targets = fopen(filename,'r');
targets = fscanf(file_targets, '%f');
targets = reshape(targets, [nb_joints, numel(targets) / nb_joints])';
fclose(file_states);

filename = './output/act_joints.txt';
file_actuals = fopen(filename,'r');
actuals = fscanf(file_actuals, '%f');
actuals = reshape(actuals, [nb_joints, numel(actuals) / nb_joints])';
fclose(file_actuals);

diffs = targets - actuals;

max_values = max(diffs);
min_values = min(diffs);
median_values = median(diffs);
mean_values = mean(diffs);
std_values = std(diffs);

% Plot Joint Error Range histogram for each joint
for i = 1:1:nb_joints
  histogram(diffs(:, i));
  fig_name = strcat("JER_histogram_", nb_joints);
%   saveas(gcf, fig_name, 'fig')
%   saveas(gcf, fig_name, 'epsc')
end

end