function plot_goal_steps(path, states, skin, skin_filename)

cd(path);

ee_pos_x = cell2mat(states(4));
ee_pos_y = cell2mat(states(5));
ee_pos_z = cell2mat(states(6));

endgoal_pos_x = cell2mat(states(10));
endgoal_pos_y = cell2mat(states(11));

goal_pos_x = cell2mat(states(2));
goal_pos_y = cell2mat(states(3));
%pos_z = zeros(size(pos_y, 1)); Leads to an out of memory error
goal_pos_z = goal_pos_y;
goal_pos_z(:, 1) = 0.0;

transitions = cell2mat(states(12));
actual_transitions = transpose(find(transitions == 1));

% Plots parameters 
colorMap = jet(256); % Create colormap

% Projecting 3D ee gazebo coordinates to 2D observation space
scaleFactor = .001;
cylinder_r = 100 * scaleFactor;
proj_z = 50 * scaleFactor;
% projecting cylinder
z_min = -50 * scaleFactor;
z_max = 150 * scaleFactor;
num_points = 200;

last_trans_idx = 1;
for trans_idx = actual_transitions
  path_idx_range = last_trans_idx:1:trans_idx;
  
  % Plot cylinder
  [x,y,z]=cylinder(cylinder_r, num_points);
  z(1,:) = z_min;
  z(2,:) = z_max;
  surf(x(:,1:num_points/4),y(:,1:num_points/4),z(:,1:num_points/4),'FaceColor', 'blue', 'FaceAlpha', 0.3, 'EdgeAlpha', 0.0);
  hold on;
  grid on;
  surf(x(:,num_points*3/4:end),y(:,num_points*3/4:end),z(:,num_points*3/4:end),'FaceColor', 'blue', 'FaceAlpha', 0.3, 'EdgeAlpha', 0.0);
  
  % Check if it's high-res skin, in which case draw all skin taxels
  if contains(skin_filename, "highres")
    % We already have the data in the skin variable, we just plot them with
    % the handle and setting the flag
    plot3(skin(:,1), zeros(size(skin(:,1))), skin(:,2), '.',  'color', 'black');
    hold on;
  end
  
  i = 1;
  for path_idx = path_idx_range
    if goal_pos_x(path_idx) ~= 9999.0
      plot3(goal_pos_x(path_idx), goal_pos_z(path_idx), goal_pos_y(path_idx), 'o', 'MarkerFaceColor', 'blue', 'color', 'blue')
      txt = strcat("\leftarrow ", int2str(i));
      text(goal_pos_x(path_idx), goal_pos_z(path_idx), goal_pos_y(path_idx), txt, 'color', 'blue');
    end
    if ee_pos_x(path_idx) ~= 9999.0
      v = [ee_pos_x(path_idx) ee_pos_y(path_idx)];
      v = v / norm(v) * cylinder_r;
      v2 = [v(1) v(2) 0];
      u2 = [ee_pos_x(path_idx) ee_pos_y(path_idx) (ee_pos_z(path_idx) - proj_z)];
      cos_val = dot(v2,u2)/(norm(u2)*norm(v2));
      length = cylinder_r / cos_val;
      v = u2 / norm(u2) * length;
      v(3) = proj_z+v(3);
      % Project on skin map
      v2 = [v(1) v(2)];
      u2 = [cylinder_r 0];
      angle = acos(dot(v2,u2)/(norm(u2)*norm(v2)));
      new_x = cylinder_r * angle * sign(v(2));
      new_y = proj_z;
      
      plot3(new_x, goal_pos_z(path_idx), new_y, 'o', 'MarkerFaceColor', 'magenta', 'color', 'magenta')
      txt = strcat("\leftarrow ", int2str(i));
      text(new_x, goal_pos_z(path_idx), new_y, txt, 'color', 'magenta');
    end
    i = i + 1;
  end
  last_trans_idx = trans_idx;
  
  if endgoal_pos_x(path_idx) ~= 9999.0
      plot3(endgoal_pos_x(path_idx), goal_pos_z(path_idx), endgoal_pos_y(path_idx), 'o', 'MarkerFaceColor', 'red', 'color', 'red')
  end
  
  % Figure settings
  % have to be re-applied, otherwise it applies only on the first figure
  set(gca, 'FontSize', 26);
  set(gcf, 'Position', get(0, 'Screensize'));
  
  close all;
end

end