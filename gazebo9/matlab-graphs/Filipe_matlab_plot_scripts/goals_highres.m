clc;
clear all;
close all;

% Create colormap
colorMap = jet(256);

% Get skin coordinates
formatSpec = '%f';
skin_filename = '../coordinate-maps/highres-torso.txt';
fileID = fopen(skin_filename,'r');
skin = fscanf(fileID, formatSpec);
skin = reshape(skin, [2, numel(skin) / 2])';
fclose(fileID);

% Get goals
formatSpec = '%f';
goal_filename = '../datasets/exp043cb10/output/goals.txt';
fileID = fopen(goal_filename,'r');
goalss = fscanf(fileID, formatSpec);
goalss = reshape(goalss, [2, numel(goalss) / 2])';
fclose(fileID);

% get reaching error distance
filename = '../datasets/exp043cb10/output/data-1000.txt';
fileID = fopen(filename,'r');
errors = fscanf(fileID, formatSpec);
errors = reshape(errors, [5, numel(errors) / 5])';



figure
hold on
grid on
i = 0;
none_index = [];
h = zeros(4, 1);
h(1) = scatter(goalss(:,1),goalss(:,2), [], [.7,.7,.7], 'filled');
h(2) = scatter(skin(:,1),skin(:,2), 'b', 'filled');

i = 0;
for i = 1:size(errors, 1)
  if errors(i, 5) < 9999
    h(4) = viscircles([errors(i, 1) errors(i, 2)],abs(errors(i, 5))/5, 'Color', 'red');
  else
    none_index = [none_index; errors(i, 1) errors(i,2)];
  end
end
h(3) = scatter(none_index(:,1), none_index(:,2), 'black', 'filled');
legend(h, 'Goals','Artificial skin taxels', 'Non-reached taxels', 'Mean Reaching Error / 5');
%legend(h, 'Goals','Artificial skin taxels', 'Mean Reaching Error / 5');
title('Distribution of goals in Random Goal Babbling')

x_min = min(skin(:,1));
x_max = max(skin(:,1));
y_min = min(skin(:,2));
y_max = max(skin(:,2));
dx = x_max - x_min;
dy = y_max - y_min;

xlim([x_min-dx x_max+dx]);
ylim([y_min-dy y_max+dy]);

xticks([x_min-dx x_min x_max x_max+dx]);
yticks([y_min-dy y_min y_max y_max+dy]);
