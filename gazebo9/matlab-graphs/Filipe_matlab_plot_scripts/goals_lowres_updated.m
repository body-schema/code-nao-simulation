clc;
clear all;
close all;
set(gca, 'FontSize', 26);
set(gcf, 'Position', get(0, 'Screensize'));
ay=gca;
ay.YAxis.TickLabelFormat = '%.2f';
ay.XAxis.TickLabelFormat = '%.2f';

% Create colormap
colorMap = jet(256);

% Get skin coordinates
formatSpec = '%f';
skin_filename = '../coordinate-maps/lowres-torso.txt';
fileID = fopen(skin_filename,'r');
skin = fscanf(fileID, formatSpec);
skin = reshape(skin, [2, numel(skin) / 2])';
fclose(fileID);

% Get goals
formatSpec = '%f';
goal_filename = '../datasets/exp003cb3_2/output/goals.txt';
fileID = fopen(goal_filename,'r');
goalss = fscanf(fileID, formatSpec);
goalss = reshape(goalss, [2, numel(goalss) / 2])';
fclose(fileID);

% get reaching error distance
filename = '../datasets/exp003cb3_2/output/data-1000.txt';
fileID = fopen(filename,'r');
errors = fscanf(fileID, formatSpec);
errors = reshape(errors, [5, numel(errors) / 5])';
fclose(fileID);


% edges for better visualisation
% edges torso low res
edges = [
%     1 2; 2 3; 3 4; 4 5; 5 6; 6 7; 7 8; 8 9; 9 10;
%     1 11; 11 21; 21 31; 31 41; 41 51; 51 61; 61 71; 71 81; 81 91; 
    1 5; 1 6; 2 6; 2 7; 3 7; 3 8; 4 8; 4 9;
    5 10; 6 11; 7 12; 8 13; 9 14;
    10 15; 11 15; 11 16; 12 16; 12 17; 13 17; 13 18; 14 18;
    15 19; 16 20; 17 21; 18 22;
    19 23; 20 23; 20 24;21 24; 21 25; 22 25
];

% edges head low res
% edges = [
%       1 2; 1 7; 2 3; 3 4; 3 9; 4 5; 5 11;
%       6 7; 6 18; 7 8; 8 9; 9 10; 10 11; 11 12; 12 24;
%       13 14; 13 19; 14 15; 15 16; 15 21; 16 17; 17 23;
%       18 19; 19 20; 20 21; 21 22; 22 23; 23 24;
% ];


hold on
grid on
i = 0;
none_index = [];
h = zeros(4, 1);
h(1) = scatter(goalss(:,1),goalss(:,2), [], [.7,.7,.7], 'filled');
% h(2) = scatter(skin(:,1),skin(:,2), 'b', 'filled');
 for i = 1:size(edges,1)
 	plot([skin(edges(i,1),1) skin(edges(i,2),1)], [skin(edges(i,1),2) skin(edges(i,2),2)], 'Color', [.7,.7,1]);
 end
 i = 0;
 for i = 1:size(errors, 1)
   if errors(i, 5) == 0.0
     viscircles([errors(i, 1) errors(i, 2)], 0.003, 'Color', 'blue');
   elseif errors(i, 5) < 9999
     viscircles([errors(i, 1) errors(i, 2)],abs(errors(i, 5)), 'Color', 'magenta');
   else
     viscircles([errors(i, 1) errors(i, 2)], 0.003, 'Color', 'red');
   end
 end
 for i = 1:size(errors, 1)
   if errors(i, 5) == 0.0
     h(2) = scatter(errors(i,1), errors(i, 2), 'blue', 'filled');
   elseif errors(i, 5) < 9999
     h(4) = scatter(errors(i,1), errors(i, 2), 'magenta', 'filled');
   else
     h(3) = scatter(errors(i,1), errors(i,2), 'red', 'filled');
   end
 end
legend(h, 'Goals', 'Reached taxels', 'Unreached', 'Reached with error', 'FontSize', 33, 'Location','NorthEast');
%title('Distribution of goals in Direct Optimization Discrete Goal Babbling, 32x32')

x_min = min(skin(:,1));
x_max = max(skin(:,1));
y_min = min(skin(:,2));
y_max = max(skin(:,2));
dx = x_max - x_min;
dy = y_max - y_min;

xlim([x_min-dx x_max+dx]);
ylim([y_min-dy y_max+dy]);

%xticks([x_min-dx x_min x_max x_max+dx]);
%yticks([y_min-dy y_min y_max y_max+dy]);

%xticks([x_min-dx:0.05:x_max+dx]);
%yticks([y_min-dy:0.05:y_max+dy]);

axis equal
% Random GB
saveas(gcf,'RandomGoalBabbling_cb3_2.fig')
saveas(gcf,'RandomGoalBabbling_cb3_2', 'epsc')
% % Discrete GB 15
% saveas(gcf,'DiscretisedGoalBabbling15_cb10.fig')
% saveas(gcf,'DiscretisedGoalBabbling15_cb10', 'epsc')
% % Discrete GB 32
% saveas(gcf,'DiscretisedGoalBabbling32_cb10.fig')
% saveas(gcf,'DiscretisedGoalBabbling32_cb10', 'epsc')
% % Direct Opti RGB
% saveas(gcf,'DirectOptimizedRandomGoalBabbling.fig')
% saveas(gcf,'DirectOptimizedRandomGoalBabbling', 'epsc')
% % Tree (Sagg-riac)
% saveas(gcf,'Tree.fig')
% saveas(gcf,'Tree', 'epsc')
% % Direct Opti DGB 32
% saveas(gcf,'DirectOptimizedDiscretisedGB32_cb10.fig')
% saveas(gcf,'DirectOptimizedDiscretisedGB32_cb10', 'epsc')