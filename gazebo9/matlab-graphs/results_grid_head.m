clc;
clear all;
close all;


% Plot skin
formatSpec = '%f';
skin_filename = 'highres-head.txt';
fileID = fopen(skin_filename,'r');
skin = fscanf(fileID, formatSpec);
skin = reshape(skin, [2, numel(skin) / 2])';
fclose(fileID);

scatter(skin(:,1),skin(:,2), 'black', 'filled');
hold on

% Grid edges
edges = [];

% Targets
targets = [];
% targets = [1:10];
% targets = [targets 11:20];
% targets = [targets 21:30];
% targets = [targets 31:40];
% targets = [targets 41:50];
% targets = [targets 51:60];
% targets = [targets 61:70];
% targets = [targets 71:80];
% targets = [targets 81:90];
% targets = [targets 91:100];
% targets = [targets 101:110];
% targets = [targets 111:120];
%targets = [targets 121:130];
%targets = [targets 131:140];
% targets = [targets 141:150];
% targets = [targets 151:160];
% targets = [targets 161:170];
% targets = [targets 171:180];
% targets = [targets 181:190];
% targets = [targets 191:200];
% targets = [targets 201:210];
% targets = [targets 211:220];
% targets = [targets 221:230];
% targets = [targets 231:240];

%% Original
% targets = [2 34 43 51 57 59 74 101 112 126 154 165 171 177 179 194 227 236];
% 
% scatter(skin(targets,1), skin(targets,2), 'blue', 'filled');
% 
% edges = [
%     % Horizontal
%     2 34; 2 57; 57 59; 59 51; 34 43; 51 74; 74 101; 101 112; 59 179; 171 179;
%     177 179; 171 126; 177 194; 126 154; 154 165; 194 227; 227 236;
%     % Vertical
%     2 74; 51 57; 34 101; 43 112; 171 177; 126 194; 154 227; 165 236; 51 177;
%     74 194; 101 227; 112 236;
% ];

%% + 2 on each edges
targets = [2 8 18 34 43 51 57 59 74 101 112 126 130 140 154 165 171 177 179 194 227 236];

scatter(skin(targets,1), skin(targets,2), 'blue', 'filled');

edges = [
    % Horizontal
    2 34; 2 57; 57 59; 59 51; 34 43; 51 74; 74 101; 101 112; 59 179; 171 179;
    177 179; 171 126; 177 194; 126 154; 154 165; 194 227; 227 236; 57 8; 8 18;
    18 34; 171 130; 130 126; 130 140; 140 154;
    % Vertical
    2 74; 51 57; 34 101; 43 112; 171 177; 126 194; 154 227; 165 236; 51 177;
    74 194; 101 227; 112 236; 8 2;
];

for i = 1:size(edges,1)
	plot([skin(edges(i,1),1) skin(edges(i,2),1)], [skin(edges(i,1),2) skin(edges(i,2),2)], 'Color', [0.7,.7,1]);
end

% Plot targets once again (needed for proper legend)
scatter(skin(targets,1), skin(targets,2), 'blue', 'filled');

legend('Skin taxels', 'Target goals') %, 'Location', 'southeast');

% title('Incidence graph')
xlabel('X [m]')
ylabel('Y [m]')
title('Target goal grid')

