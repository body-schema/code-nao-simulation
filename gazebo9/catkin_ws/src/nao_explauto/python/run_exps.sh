#!/bin/bash

# if virtualenvwrapper.sh is in your PATH (i.e. installed with pip)
# source to make it usable in the script
source `which virtualenvwrapper.sh`
workon gazebop3

# Variables: to change for each set of experiment to run
experiment="./exp0611-matthias-plannar-nn-torso-rhand.py"
expNB="exp0611"

# Create the directory for the expriment
mkdir -p results_stats/${expNB}


#for i in {5..10}
for i in 1
  do

  # Create the directory for the run
  mkdir -p results_stats/${expNB}/${expNB}_${i}
  mkdir -p output
  mkdir -p models
  
  
  SECONDS=0
  python ${experiment}
  elapsedseconds=$SECONDS
  
  echo "$(($elapsedseconds / 3600)):$((($elapsedseconds / 60) % 60)):$(($elapsedseconds % 60))" > output/real_time_spent.txt

  # Move the results from the default folder to the
  # experiment-specific folder
  mv output results_stats/${expNB}/${expNB}_${i}/
  mv models results_stats/${expNB}/${expNB}_${i}/
done
