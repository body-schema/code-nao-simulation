from gazebo_link_position import *

if __name__ == "__main__":
    getter = GazeboLinkPosition("lowres") # Parameter should be "lowres" or "highres"
    
    print("EE", getter.get_link_position("nao_skin::l_wrist_pen_point"))

    # print("Taxel 0 on torso (old method)", getter.get_link_position("nao_skin::torso_skin_coords_0"))
    print("Taxel 0 on torso (new method)", getter.get_taxel_position("torso", 0))
    
    # print("Taxel 0 on Head (old method)", getter.get_link_position("nao_skin::Head_skin_coords_0"))
    print("Taxel 0 on Head (new method)", getter.get_taxel_position("head", 0))
    
    # print("Taxel 0 on l_wrist (old method)", getter.get_link_position("nao_skin::l_wrist_skin_coords_0"))
    print("Taxel 0 on l_wrist (new method)", getter.get_taxel_position("l_wrist", 0))

    # print("Taxel 0 on r_wrist (old method)", getter.get_link_position("nao_skin::r_wrist_skin_coords_0"))
    print("Taxel 0 on r_wrist (new method)", getter.get_taxel_position("r_wrist", 0))
