# General imports
import numpy as np
import pickle
from math import pi, sqrt, acos, asin, ceil
from matplotlib import pyplot
from mpl_toolkits.mplot3d import Axes3D
from operator import add

# ROS and Gazebo imports
import rospy
from gazebo_link_pose import *
from gazebo_msgs.srv import GetJointProperties, GetJointPropertiesResponse, GetLinkState
from nao_ml_inc import *
from std_msgs.msg import Float64

# Explauto/Environment imports
from explauto import Environment
from nao_explauto_env_3 import NaoEnvironment

# Matthias' model/gb import
from gb import GoalBabbling
from models import LinearModel, LocalLinearModel

def test_data():
    explauto_env.use_reset = True
    filename_reaching = 'output/data-' + str(i) + '.txt'
    fh_reaching = open(filename_reaching, 'w')
    print('Test learnt model')
    # Try to reach each of the taxels
    for k in skin_map:
        s_g = skin_map[k]
        s = None
        best_dist = float("inf")
        # Best of 2, just to be sure
        for kk in range(2):
            m = model.predict(s_g)
            s_t, timeSpent = explauto_env.compute_sensori_effect(m)
            if s_t is not None:
                dist = ((s_g[0] - s_t[0])**2 + (s_g[1] - s_t[1])**2)**0.5
                if dist < best_dist:
                    s = s_t[:]
                    best_dist = dist
        print('Expected observation: ' + ','.join(str(x) for x in s_g))
        if s is None:
            print('Actual observation:   None')
            fh_reaching.write(str(s_g[0]) + ' ' + str(s_g[1]) + ' ' + str(9999) + ' ' + str(9999) + ' ' + str(9999) + ' ' + str(timeSpent) + '\n')
        else:
            print('Actual observation:   ' + ','.join(str(x) for x in s))
            print('Distance:   ' + str(best_dist))
            fh_reaching.write(str(s_g[0]) + ' ' + str(s_g[1]) + ' ' + str(s[0]) + ' ' + str(s[1]) + ' ' + str(best_dist) + ' ' + str(timeSpent) + '\n')
        #raw_input("Press Enter to continue...")
        print('')
    fh_reaching.close()
    explauto_env.use_reset = use_reset
    return


# Misc. Variable definitions
collision_target_link = 'base_link' # Target link on which collisions are detected
ee_link_name = 'nao_skin::r_wrist' # End effector link name
ee_link_state_name = "nao_skin::l_wrist_pen_point" # ee added link for 3D coordinates state
max_learning_iterations = 50000
nb_observation = 0 # Monitor amount of observations during exploration
reference_frame = "world" # reference frame for 3D coordinates
ros_topic = '/gazebo_contact_info/r_wrist' # ROS topic name for Gazebo contacts
total_sensors = 25 # Number of sensors
use_reset = False # Defines if we want to reset simulation when applying motor commands


# Save / Load model
load_model = False # If True, just test saved model
save_model = True # If true, save learnt model to disk

## Home posture

# A nice choice (in contact with surface, generates an outcome)
home_pose = [1.525, 0.499, 0.541, -0.03, 0.764]

# Far from surface (no outcome generated)
#home_pose = [1.495, 1.539, 0.083, -0.02, 0.109]

#home_pose_old = [1.48, -0.45, 0, -0.1, np.pi/2]
zero_pose = [0, 0, 0, 0, 0]

# Joint data structure
joints = [
    {
        "joint_name": "RElbowRoll",
        "publisher": None,
        "limit_min": 0.0349066,
        "limit_max": 1.54462
    },
    {
        "joint_name": "RElbowYaw",
        "publisherg": None,
        "limit_min": -2.08567,
        "limit_max": 2.08567
    },
    {
        "joint_name": "RShoulderPitch",
        "publisher": None,
        "limit_min": -2.08567,
        "limit_max": 2.08567
    },
    {
        "joint_name": "RShoulderRoll",
        "publisher": None,
        "limit_min": -1.32645,
        "limit_max": 0.314159
    },
    {
        "joint_name": "RWristYaw",
        "publisher": None,
        "limit_min": -1.82387,
        "limit_max": 1.82387
    },
]

# Skin to coordinates mapping
skin_map = {}
map_fh = open('coordinate-maps/lowres-torso.txt', 'r')
for i in range(total_sensors):
    k = str(i) + '_' + str(i + 2)
    skin_map[k] = [float(x) for x in map_fh.readline().strip().split(" ")]
map_fh.close()

# Initialize ROS
rospy.init_node('nao_skin_ml', anonymous=True)
srv_joint_state = rospy.ServiceProxy('/gazebo/get_joint_properties', GetJointProperties)
rospy.wait_for_service('gazebo/get_link_state')
state_getter = rospy.ServiceProxy('gazebo/get_link_state', GetLinkState)

# Initialize joints
for joint in joints:
    topic_name = '/nao_dcm/' + joint['joint_name'] + '_position_controller/command';
    joint['publisher'] = rospy.Publisher(topic_name, Float64, queue_size=5)

# Initialize explauto environment
joint_min = [x['limit_min'] for x in joints]
joint_max = [x['limit_max'] for x in joints]

# Set bounds of sensory space
min_x = min([skin_map[k][0] for k in skin_map])
max_x = max([skin_map[k][0] for k in skin_map])
min_y = min([skin_map[k][1] for k in skin_map])
max_y = max([skin_map[k][1] for k in skin_map])
dx = (max_x - min_x)
dy = (max_y - min_y)
s_mins = [min_x - dx, min_y - dy]
s_maxs = [max_x + dx, max_y + dy]

# Create targets for goal generation
targetList = []
for x in np.linspace(min_x, max_x, 10):
    for y in np.linspace(min_y, max_y, 10):
        targetList.append(np.array([x,y]))

# Create Nao environment (inheritence explauto's 'Environment' class)
explauto_env = NaoEnvironment(joint_min, joint_max, s_mins, s_maxs, joints, ros_topic, ee_link_name, use_reset, skin_map, collision_target_link)

# Setup inverse model:
M = len(joints) # Output dimension (motor command)
N = 2 # Input dimension (goal coordinates in 2d space)
model = LocalLinearModel(N, M, dist=0.05)

# Search for a taxel close to the home posture if the home posture
# doesn't naturally have a sensory effect
explauto_env.use_reset = True
#explauto_env.compute_sensori_effect(zero_pose)
print("Trying to reach home")
home_se, timeSpent = explauto_env.compute_sensori_effect(home_pose)
print("Home reached")
if home_se is None and home_contact_mode == 1:
    while home_se is None:
        home_se, timeSpent = explauto_env.compute_sensori_effect(home_pose)
        startGoal = home_se
if home_se is None:
    closestPosture = None
    closestOutcome = None
    closestDistance = float("Inf")
    t = 0
    while t < 100 or closestOutcome is None:
        randomMotorCommand = home_pose + np.random.randn(M)*0.3
        np.maximum(randomMotorCommand, joint_min, randomMotorCommand)
        np.minimum(randomMotorCommand, joint_max, randomMotorCommand)
        outcome, timeSpent = explauto_env.compute_sensori_effect(randomMotorCommand)
        print(t)
        if outcome is not None:
            d = np.linalg.norm(randomMotorCommand-home_pose)
            if d < closestDistance:
                closestPosture = randomMotorCommand
                closestOutcome = outcome
                closestDistance = d
        t += 1
    print("Init posture:" + str(closestPosture))
    print("Init position:" + str(closestOutcome))
    startGoal = closestOutcome
    model.adapt(closestOutcome, closestPosture, 1.0)
    model.offsets[0] = closestPosture
else:
    model.adapt(home_se, home_pose, 1.0)
    model.offsets[0] = home_pose
explauto_env.compute_sensori_effect(home_pose)
explauto_env.use_reset = use_reset

# Setup exploration method
goHomeProbability=0.1
goalStepLength=0.01
motorStepLength=0.1
gb = GoalBabbling(explauto_env, model, home_pose, targetList, startGoal=startGoal,
                  goHomeProbability=goHomeProbability, goalStepLength=goalStepLength,
                  motorStepLength=motorStepLength)
gb.setMotorPerturbation(0.01)

# Initialization complete
print("NAO Skin ML: Initialization complete")
print("Home posture:" + str(home_pose))
print("Home position:" + str(home_se))
print("Start goal:" + str(startGoal))

# Motor babbling
if load_model:
    # Load saved model from disk
    filename = 'models/model-1000.sav'
    i = 1000
    fh = open(filename, 'rb')
    model = pickle.load(fh)
    fh.close()
    test_data()
else:
    i = 0
    fh_goals = open('output/goals.txt', 'w')
    fh_motors = open('output/motors.txt', 'w')
    fh_states = open('output/states.txt', 'w')
    fh_times = open('output/times.txt', 'w')
    sample = gb.generateSample() #first = generate home posture
    while (not rospy.is_shutdown()) and (i < max_learning_iterations):
        # Generate goal
        sample = gb.generateSample()
        logState = gb.logState
        if sample.target is not None:
            fh_goals.write(' '.join(map(str, sample.target)) + '\n')
            print('Generated goal: ' + ','.join(str(x) for x in sample.target))
        fh_times.write(str(sample.timeSpent) + '\n')
        fh_motors.write(' '.join(map(str, sample.motor)) + '\n')
        i = i + 1

        state_array = np.empty(12)
        state_array[0] = i # iteration
        if logState.interGoal is not None: # Intermediary goal in skin 2D space
            state_array[1] = logState.interGoal[0]
            state_array[2] = logState.interGoal[1]
        else:
            state_array[1] = 9999
            state_array[2] = 9999
        try: # End Effector 3D coordinates in gazebo space
            ee_link_response = state_getter(ee_link_state_name, reference_frame)
            ee_3D_position = ee_link_response.link_state.pose.position
            state_array[3] = ee_3D_position.x
            state_array[4] = ee_3D_position.y
            state_array[5] = ee_3D_position.z
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e)
            state_array[3] = 9999
            state_array[4] = 9999
            state_array[5] = 9999
        if logState.outcome is not None: # Sensory feedback/touched taxel in skin 2D space
            state_array[6] = logState.outcome[0]
            state_array[7] = logState.outcome[1]
        else:
            state_array[6] = 9999
            state_array[7] = 9999
        state_array[8] = logState.weight # Learning rate weight coefficient
        if logState.endGoal is not None: # end goal in skin 2D space
            state_array[9] = logState.endGoal[0]
            state_array[10] = logState.endGoal[1]
        else:
            if startGoal is not None:
                state_array[9] = startGoal[0]
                state_array[10] = startGoal[1]
            else:
                state_array[9] = 9999
                state_array[10] = 9999
        state_array[11] = logState.endGoalChangeNext #Flag for class transition next iteration
        
        fh_states.write(' '.join(map(str, state_array)) + ' ' + logState.className + '\n')

        print('Motor command: ', i, [round(x, 2) for x in sample.motor])
        if sample.outcome is None:
            print('Observation: None')
        else:
            # Learning
            model.adapt(sample.outcome, sample.motor, 1 * sample.weight)
            nb_observation = nb_observation + 1
            print('Observation: ' + ','.join(str(x) for x in sample.outcome))
        print('')

        # Every $save_every_n$ iterations:
        if (i < 1000 and i % 100 == 0) or (i % 1000 == 0):
            if save_model:
                filename = 'models/model-' + str(i) + '.sav'
                fh = open(filename, 'wb')
                pickle.dump(model, fh)
                fh.close()

        if (i % 100 == 0):
            explauto_env.use_reset = True
            explauto_env.compute_sensori_effect(home_pose)
            explauto_env.use_reset = use_reset

        # Test learnt model - 10x10 grid
        if (i % 100 == 0 and i < 1000) or (i % 1000 == 0):
            test_data()
            # Save the number of touches too
            fh = open('output/touches.txt', 'a')
            fh.write(str(nb_observation) + '\n')
            fh.close()

    fh_endgoals = open('output/end_goals.txt', 'w')
    fh_endgoals.write(str(gb.numCompletedMovements) + '\n')
    fh_endgoals.close()
    fh_goals.close()
    fh_motors.close()
    fh_states.close()
    fh_times.close()
