import rospy
import tf2_ros
import tf2_geometry_msgs
from gazebo_msgs.srv import GetLinkState
from geometry_msgs.msg import TransformStamped
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Vector3

class GazeboLinkPosition:
    body_parts = ["head", "l_wrist", "r_wrist", "torso"]
    body_parts_map = {
        "head": "nao_skin::Head",
        "l_wrist": "nao_skin::l_wrist",
        "r_wrist": "nao_skin::r_wrist",
        "torso": "nao_skin::base_link"
    }
    # Just FYI: this is from URDF
    body_parts_offsets = {
        "head": [0, 0, 0.01],
        "l_wrist": [-0.025, 0, 0],
        "r_wrist": [-0.025, 0, 0],
        "torso": [0, 0, 0.01]
    }
    reference_frame = "world"
    state_getter = None
    taxel_coordinates = {}
    # Version should be "lowres" or "highres"
    version = None

    def __init__(self, version):
        self.version = version

        for body_part in self.body_parts:
            self.taxel_coordinates[body_part] = []
            file_name = "taxel-coordinates/" + version + "-" + body_part + ".txt"
            map_fh = open(file_name, 'r')
            lines = map_fh.readlines()
            map_fh.close()
            for line in lines:
                if line.strip():
                    self.taxel_coordinates[body_part].append([float(x) for x in line.strip().split(" ")])

        try:
            rospy.wait_for_service('gazebo/get_link_state')
            self.state_getter = rospy.ServiceProxy('gazebo/get_link_state', GetLinkState)
        except rospy.ServiceException as e:
            print("Service instantiation failed: %s"%e)

    def get_link_position(self, link_name):
        try:
            response = self.state_getter(link_name, self.reference_frame)
            return response.link_state.pose.position
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e)

    def get_taxel_position(self, body_part, taxel_index):
        try:
            link_name = self.body_parts_map[body_part]
            response = self.state_getter(link_name, self.reference_frame)

            # Target frame position
            p = response.link_state.pose.position
            # Target frame orientation
            o = response.link_state.pose.orientation

            ts = TransformStamped()
            ts.transform.rotation = o
            ps = PoseStamped()
            taxel = self.taxel_coordinates[body_part][taxel_index]
            offset = self.body_parts_offsets[body_part]
            ps.pose.position.x = taxel[0] + offset[0]
            ps.pose.position.y = taxel[1] + offset[1]
            ps.pose.position.z = taxel[2] + offset[2]
            pt = tf2_geometry_msgs.do_transform_pose(ps, ts)
            pt = pt.pose.position
            
            v = Vector3(p.x + pt.x, p.y + pt.y, p.z + pt.z)

            return v
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e)
