function touches_global_figure(path)

if nargin < 1
  %path = "../results_finished/";
  %path = "../results_jan2021_test_maxmodifs/";
  %path = "../results_feb2021_aft_review/";
  %path = "../results_feb2021_aft_review_3Deefix/";
  path = "../results_final_tcds";
end

init_path = pwd;
%% Init
% List of set of exps folders
cd(path); % change dir
folders_struct = dir; % get list of set of exps folders
folders_names_cells = {folders_struct.name}; % get cell array of names
folders = string(folders_names_cells(3:end)); % into string array, ignore . and ..
% only take folders that start with 'exp'
cleaned_folders_idx = regexp(folders, regexptranslate('wildcard', 'exp*'));
cleaned_folder_idx_bin = zeros(1, size(cleaned_folders_idx, 2));
for i = 1:1:size(cleaned_folders_idx, 2)
  cleaned_folder_idx_bin(i) = cleaned_folders_idx{i};
end
folders = folders(cleaned_folder_idx_bin == 1);

% Plots parameters 
data_range = 100:100:1000; % test checkpoints (default: every 100 iterations)
co = get(gca,'ColorOrder'); % Initial colors
newColors = [co; 0 0 0; 1 0 0; 1 1 0; 1 0 1;]; % Add new colors for more functions on the same plot

%% Processing

% row = 1:6 lowres torso, 7:12 lowres head, 13:16 highres torso, 17:20 highres head
exp_mean_plots = zeros(size(folders, 2), size(data_range, 2));
fold_idx = 1;

% For each set of experiment
for fold = folders
  % Figure settings
  % have to be re-applied, otherwise it applies only on the first figure
  set(gca, 'FontSize', 31);
  set(gcf, 'Position', get(0, 'Screensize'));
  % This sets the new colour scheme and automatically opens a figure with it.
  set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
  
  % Figure labels and settings
  grid on
  xlabel('Iteration number')
  ylabel('Number of touches')
  %title('Comparison of same-exp results')
 
  cd(fold);
  exp_folds_struct = dir;  % get list of individual exps folders
  exp_names_cells = {exp_folds_struct.name}; % get cell array of names
  exps = string(exp_names_cells(3:end)); % into string array, ignore . and ..
  % only take folders that start with 'exp'
  cleaned_exps_idx = regexp(exps, regexptranslate('wildcard', 'exp*'));
  cleaned_exps_idx_bin = zeros(1, size(cleaned_exps_idx, 2));
  for i = 1:1:size(cleaned_exps_idx, 2)
    cleaned_exps_idx_bin(i) = cleaned_exps_idx{i};
  end
  exps = exps(cleaned_exps_idx_bin == 1);
  
  all_touches = zeros(size(exps, 2), size(data_range, 2));
  exp_idx = 1;
  
  % For each individual experiment, get the results and plot them
  for exp = exps
    filename = strcat(exp, '/output/touches.txt');
    fileID = fopen(filename,'r');
    data = fscanf(fileID, '%i');
    %data = data(end-size(data_range, 2)+1:end); % to plot pbgb with 15000
    data = data(1:size(data_range, 2)); % to plot pbgb with 1000
    plot(data_range, data(1:size(data_range, 2)), '--', 'LineWidth', 3);
    hold on;
    
    all_touches(exp_idx, 1:end) = transpose(data);
    exp_idx = exp_idx + 1;
    
    fclose(fileID);
  end
  
  touches_mean = mean(all_touches);
  final_std = std(all_touches);
  plot(data_range, touches_mean, 'LineWidth', 4);
  errorbar(data_range, touches_mean, final_std, 'Color', [1 0 1], 'LineWidth', 1.5);
  
  % Figure legend and settings
  %legend(labels, 'FontSize', 31, 'Location','NorthWest')
  axis([data_range(1) data_range(end)  0 data_range(end)]);
  %yticks([0:200:1000]);
  %xticks([100 300 500 800 1000]);
  
  % Save figure
  fig_name = strcat('nbTouches_', fold, '_all');
  %saveas(gcf, fig_name, 'fig')
  %saveas(gcf, fig_name, 'epsc')
  
  exp_mean_plots(fold_idx, 1:end) = touches_mean;
  fold_idx = fold_idx + 1;
  
  cd ..;
  close all;
end

cd("../TCDS_results");
% row = 1:6 highres torso, 7:11 highres head
%% Plot final torso highres
% Figure settings
set(gca, 'FontSize', 48);
set(gcf, 'Position', get(0, 'Screensize'));
set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
labels = ["RMB" "RGB" "DGB" "DGB-nr", "DMB", "PBCGB"];
%labels = ["ac-fa" "ac-kino" "ac-rcPfa" "ac-r", "ac-ur", "ac-vs", "ac-vsPfaPrc-dart", "ac-vsPfaPrc", "bc-kino", "bc-ur"];

plot(data_range, exp_mean_plots(1:6, 1:end), 'LineWidth', 5);
%errorbar(data_range, exp_mean_plots(1:6, 1:end), std_mean_plots(1:6, 1:end), 'Color', [1 0 1], 'LineWidth', 1.5);
grid on
xlabel('Iteration number')
ylabel('Number of touches')
%title('Comparison of exploration strategies, torso & right hand - Low res skin')
legend(labels, 'FontSize', 60, 'Location','NorthWest');
axis([100 1000  0 inf]);

fig_name = strcat('nb_touches_torso_1k');
saveas(gcf, fig_name, 'fig')
saveas(gcf, fig_name, 'epsc')

%% Plot final head highres
% Figure settings
set(gca, 'FontSize', 48);
set(gcf, 'Position', get(0, 'Screensize'));
set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
labels = ["DGB" "DGB-nohj" "DGB-nr" "PBCGB" "PBCGB-incgs"];
%labels = ["DGB" "DGB-noheadjoints" "PBCGB" "PGCGB-incgs"];


plot(data_range, exp_mean_plots(7:11, 1:end), 'LineWidth', 5);
%errorbar(data_range, exp_mean_plots(7:10, 1:end), std_mean_plots(7:10, 1:end), 'Color', [1 0 1], 'LineWidth', 1.5);
grid on
xlabel('Iteration')
ylabel('Number of touches')
%title('Comparison of exploration strategies, torso & right hand - Low res skin')
legend(labels, 'FontSize', 60, 'Location','NorthWest');
axis([100 1000  0 inf]);

fig_name = strcat('nb_touches_head_1k');
saveas(gcf, fig_name, 'fig')
saveas(gcf, fig_name, 'epsc')

%% end, cleaning and reseting path
% For convenience, go back to the scripts folder
close all;
cd(init_path);
clear;
end