filename = '../coordinate-maps/highres-head-planar.txt';
fh_skin = fopen(filename,'r');
skin = fscanf(fh_skin, '%f %f');
skin = reshape(skin, [2, numel(skin) / 2])';
fclose(fh_skin);

scatter(skin(:,1), skin(:,2), '.', 'black');
hold on;
grid on;

disp("coucou");
  