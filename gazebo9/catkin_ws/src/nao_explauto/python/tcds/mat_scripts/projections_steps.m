function projections_steps(path, skin_id)

if nargin < 2
  skin_id = 2;
  if nargin < 1
    %path = "../results_finished/";
    %path = "../results_jan2021_test_maxmodifs/";
    %path = "../results_feb2021_aft_review/";
    %path = "../results_feb2021_aft_review_3Deefix/";
    %path = "../results_feb2021_aft_review_3Deefix/Compromised";
    path = "../results_final_tcds";
  end
end

%% Init
data_range = 900:100:1000; % test checkpoints (default: every 100 iterations)
%data_range = 13000:1000:15000; % test checkpoints (default: every 1000 iterations)
script_path = pwd; % save script path, to call it later
cd(path); % change dir to the one containing the experiments

% highres torso
if skin_id == 1
  folders = ["exp0401", "exp0403" 'exp0405', 'exp0405-nr', 'exp0410'];
  %folders = ["exp0411"];
  %folders = ["exp0405-dgb-planar-htorso-rhand_ac_fa_ur", "exp0405-dgb-planar-htorso-rhand_ac_kinova" 'exp0405-dgb-planar-htorso-rhand_ac_rcPfa_ur', 'exp0405-dgb-planar-htorso-rhand_ac_rc_ur', 'exp0405-dgb-planar-htorso-rhand_ac_ur', "exp0405-dgb-planar-htorso-rhand_ac_vc_kinova", "exp0405-dgb-planar-htorso-rhand_ac_vsPfaPrc_dart_kinova" 'exp0405-dgb-planar-htorso-rhand_ac_vsPfaPrc_kinova', 'exp0405-dgb-planar-htorso-rhand_bc_kinova', 'exp0405-dgb-planar-htorso-rhand_bc_ur', "exp0405-dgb-planar-htorso-rhand_ac_vsPfaPrc_1sdelay_ur", "exp0405-dgb-planar-htorso-rhand_ac_vsPfaPrc_2sdelay_kinova"];
  skin_filename = '../coordinate-maps/highres-torso-planar.txt';
% highres head
elseif skin_id == 2
  folders = ["exp0505", "exp0505-nr", "exp0505-nohj"];
  %folders = ["exp0505-nohj"];
  %folders = ["exp0511", "exp0511-incgs"];
  %folders = ["exp0511"];
  skin_filename = '../coordinate-maps/highres-head-planar.txt';
% highres head no head joints
% else % if skin_id == 3, in particular, but covers other errors/mistype/abuse
%   folders = ["exp061", "exp063", "exp064", "exp065", "exp069", "exp0610"];
%   folders = ["exp0611"];
%   skin_filename = '../coordinate-maps/highres-head.txt';
end

% Plots parameters 
colorMap = jet(256); % Create colormap

% Constants
ERROR_SCALING = 1; % Divides the reaching error by this value

% Get skin coordinates
fileID = fopen(skin_filename,'r');
skin = fscanf(fileID, '%f');
skin = reshape(skin, [2, numel(skin) / 2])';
fclose(fileID);

% Prepare axis limits from skin coordinates values
x_min = min(skin(:,1));
x_max = max(skin(:,1));
y_min = min(skin(:,2));
y_max = max(skin(:,2));
dx = x_max - x_min;
dy = y_max - y_min;


%% Processing
% For each set of experiment
for fold = folders
  cd(fold);
  exp_folds_struct = dir;  % get list of individual exps folders
  exp_names_cells = {exp_folds_struct.name}; % get cell array of names
  exps = string(exp_names_cells(3:end)); % into string array, ignore . and ..
  % only take folders that start with 'exp'
  cleaned_exps_idx = regexp(exps, regexptranslate('wildcard', 'exp*'));
  cleaned_exps_idx_bin = zeros(1, size(cleaned_exps_idx, 2));
  for i = 1:1:size(cleaned_exps_idx, 2)
    cleaned_exps_idx_bin(i) = cleaned_exps_idx{i};
  end
  exps = exps(cleaned_exps_idx_bin == 1);
  
  % For each individual experiment, get the results and plot them
  for exp = exps
    cd(exp);
    
    % Check if goals exists, if yes, load them
    if exist('./output/states.txt', 'file') == 2
      % Get goals
      goals_filename = './output/states.txt';
      fileID = fopen(goals_filename, 'r');
      states = fscanf(fileID, '%f %f %f %f %f %f %f %f %f');
      states = reshape(states, [9, numel(states) / 9])';
      goals = states(:, 2:3);
      goals = goals(goals(:, 1) ~= 9999.0, :);
      fclose(fileID);
      
      idx_endstep_goal = size(goals, 1) - floor(size(goals, 1) / 100)*100;
      last_idx_goal = 1;
    end
    
    % For each test checkpoint (default: every 100 iterations)
    for t_step = data_range
      cd("output/");
      
      % Init/reset variables
      % control variables, set to 1 when the corresponding item has been plotted once
      % Goals, Skin taxels, reached_taxels, unreached_taxels, reached_with_errors
      ctrl_handles = [0, 0, 0, 0, 0];
      handles = zeros(5); % Contains plot handles
      
      % Figure settings
      % have to be re-applied, otherwise it applies only on the first figure
      set(gca, 'FontSize', 26);
      set(gcf, 'Position', get(0, 'Screensize'));
      ay = gca;
      ay.YAxis.TickLabelFormat = '%.2f';
      ay.XAxis.TickLabelFormat = '%.2f';
      grid on;
      hold on;
      
      % Check if goals exists, if yes, plot them
      if (exist('states.txt', 'file') == 2) &&  ~isempty(goals)
        % Plot while keeping handle + set flag
        handles(1) = scatter(goals(last_idx_goal:idx_endstep_goal, 1), goals(last_idx_goal:idx_endstep_goal, 2), 100, [.7,.7,.7], 'filled');
        ctrl_handles(1) = 1;
        last_idx_goal = idx_endstep_goal;
        idx_endstep_goal = min(size(goals, 1), idx_endstep_goal + 100); % increment for next step
      end
      
      % Check if it's high-res skin, in which case draw all skin taxels
      if contains(skin_filename, "highres")
        % We already have the data in the skin variable, we just plot them with
        % the handle and setting the flag
        handles(2) = scatter(skin(:,1),skin(:,2), 150, 'black', 'filled');
        ctrl_handles(2) = 1;
      end
      
      % Get reaching errors
      filename = strcat('data-', string(t_step), '.txt');
      %filename = './output/data-1000.txt';
      fileID = fopen(filename,'r');
      errors = fscanf(fileID, '%f');
      errors = reshape(errors, [6, numel(errors) / 6])';
      fclose(fileID);
      for i = 1:size(errors, 1)
        if errors(i, 5) == 0.0
          viscircles([errors(i, 1) errors(i, 2)], 0.003, 'Color', 'blue', 'LineWidth', 5);
        elseif errors(i, 5) < 9999
          viscircles([errors(i, 1) errors(i, 2)],abs(errors(i, 5)/ERROR_SCALING), 'Color', 'magenta', 'LineWidth', 5);
        else
          viscircles([errors(i, 1) errors(i, 2)], 0.003, 'Color', 'red', 'LineWidth', 5);
        end
      end
      % Plot the dots - has to be done after circles are finished being drawn, as
      % otherwise they can overlap and hide the taxels' dots
      for i = 1:size(errors, 1)
        if errors(i, 5) == 0.0
          handles(3) = scatter(errors(i,1), errors(i, 2), 350, 'blue', 'filled');
          ctrl_handles(3) = 1;
        elseif errors(i, 5) < 9999
          handles(5) = scatter(errors(i,1), errors(i, 2), 350, 'magenta', 'filled');
          ctrl_handles(5) = 1;
        else
          handles(4) = scatter(errors(i,1), errors(i,2), 350, 'red', 'filled');
          ctrl_handles(4) = 1;
        end
      end
      
      % Set legend
      leg_idx = 1;

     % if we don't use (1,nnz), will create (nnz x nnz) matrix
      legends = strings(1, nnz(ctrl_handles)); % number of non-zero elements
      if ctrl_handles(1) == 1
        legends(leg_idx) = "Goals";
        leg_idx = leg_idx + 1;
      end
      if ctrl_handles(2) == 1
        legends(leg_idx) = "Untested taxels";
        leg_idx = leg_idx + 1;
      end
      if ctrl_handles(3) == 1
        legends(leg_idx) = "Reached taxels";
        leg_idx = leg_idx + 1;
      end
      if ctrl_handles(4) == 1
        legends(leg_idx) = "Unreached";
        leg_idx = leg_idx + 1;
      end
      if ctrl_handles(5) == 1
        if ERROR_SCALING ~= 1
          leg_err_scaled = strcat("Reached with error", '/', num2str(ERROR_SCALING));
          legends(leg_idx) = leg_err_scaled;
        else
          legends(leg_idx) = "Reached with error";
        end
      end
      handles(handles==0) = [];
      %legend(handles, legends, 'FontSize', 33, 'Location','NorthEast');
      
      % Axis settings
      % highres torso
      if skin_id == 1
        xlim([-0.10 0.10]);
        ylim([-0.01 0.08]);
      % highres head
      elseif skin_id == 2
        xlim([-0.08 0.08]);
        ylim([-0.01 0.12]);
      end
      %xlim([x_min-dx x_max+dx]);
      %ylim([y_min-dy y_max+dy]);
      axis equal
      
      % Save figure
      fig_name = strcat('data-',  string(t_step), '_Proj_', exp, '_2D');
      saveas(gcf, fig_name, 'fig')
      saveas(gcf, fig_name, 'epsc')
      
      cd ..;
      close all;
    end
    
    cd ..;
  end

  cd ..;
end

cd(script_path);
% if skin_id > 0 && skin_id < 3
%   projections_goals_stats_by100step(path, skin_id+1);
% end
clear;
end