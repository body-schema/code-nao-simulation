function plot_homeToHome_PBGB(path, states, mmd, skin, skin_filename)

cd(path);

endgoal_pos_x = cell2mat(mmd(3));
endgoal_pos_y = cell2mat(mmd(4));

goal_pos_x = cell2mat(states(2));
goal_pos_y = cell2mat(states(3));
nb_goals = size(goal_pos_x, 1);

transitions = cell2mat(mmd(5));
actual_transitions = transpose(find(transitions == 1));

classes = mmd(6);
classes = classes{1};

% Plots parameters 
colorMap = jet(256); % Create colormap

last_trans_idx = 1;
i = 1;
prev_trans = "StateGoHomePosture";
handles = zeros(1, 3);
legends = strings(1, 3);
legends(1) = "Skin taxels";
legends(2) = "Intermediary goals";
legends(3) = "End goals";
for trans_idx = actual_transitions
  
  trans = classes{trans_idx};
  
  % save plot, start a new one
  if (strcmp(prev_trans, "StateGoHomePosture")) && (~strcmp(trans, "StateGoHomePosture"))
    close all;
    i = 1;
    handles = zeros(1, 3);
    % Check if it's high-res skin, in which case draw all skin taxels
    if contains(skin_filename, "highres")
      % We already have the data in the skin variable, we just plot them with
      % the handle and setting the flag
      handles(1) = scatter(skin(:,1), skin(:,2), 50, 'black', 'filled');
      hold on;
      % Figure settings
      % have to be re-applied, otherwise it applies only on the first figure
      set(gca, 'FontSize', 35);
      set(gcf, 'Position', get(0, 'Screensize'));
      xlim([-0.08 0.08]);
      ylim([-0.01 0.08]);  
    end
    path_idx_range = last_trans_idx:1:trans_idx;
  else
    path_idx_range = last_trans_idx+1:1:trans_idx;
  end
  
  for path_idx = path_idx_range
    if goal_pos_x(path_idx) ~= 9999.0
      handles(2) = scatter(goal_pos_x(path_idx), goal_pos_y(path_idx), 150, 'blue', 'filled');
      if path_idx ~= path_idx_range(end)
        txt = strcat(" ", int2str(i));
        text(goal_pos_x(path_idx), goal_pos_y(path_idx), txt, 'color', 'blue', 'FontSize', 30);
      end
    end
    i = i + 1;
  end
  last_trans_idx = trans_idx;
  
  if endgoal_pos_x(path_idx) ~= 9999.0
    handles(3) = scatter(endgoal_pos_x(path_idx), endgoal_pos_y(path_idx), 150, 'red', 'filled');
    txt = strcat(" ", int2str(i-1));
    text(endgoal_pos_x(path_idx), endgoal_pos_y(path_idx), txt, 'color', 'red', 'FontSize', 30);
    legend(handles, legends, 'FontSize', 33, 'Location','NorthEast');
  end
  
  prev_trans = trans;
  
end
  
end
