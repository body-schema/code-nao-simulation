function plot_3D_goalsEE_PBGB(path, states, mmd, skin, skin_filename)

cd(path);

ee_pos_x = cell2mat(states(4));
ee_pos_y = cell2mat(states(5));
ee_pos_z = cell2mat(states(6));

endgoal_pos_x = cell2mat(mmd(3));
endgoal_pos_y = cell2mat(mmd(4));

goal_pos_x = cell2mat(states(2));
goal_pos_y = cell2mat(states(3));
nb_goals = size(goal_pos_x, 1);
goal_pos_z = zeros(nb_goals, 1);

transitions = cell2mat(mmd(5));
actual_transitions = transpose(find(transitions == 1));

% Plots parameters 
colorMap = jet(256); % Create colormap

last_trans_idx = 1;
for trans_idx = actual_transitions
  path_idx_range = last_trans_idx:1:trans_idx;
  
  % Check if it's high-res skin, in which case draw all skin taxels
  if contains(skin_filename, "highres")
    % We already have the data in the skin variable, we just plot them with
    % the handle and setting the flag
    plot3(skin(:,1), zeros(size(skin(:,1))), skin(:,2), '.',  'color', 'black');
    hold on;
  end
  
  i = 1;
  for path_idx = path_idx_range
    if goal_pos_x(path_idx) ~= 9999.0
      plot3(goal_pos_x(path_idx), goal_pos_z(path_idx), goal_pos_y(path_idx), 'o', 'MarkerFaceColor', 'blue', 'color', 'blue')
      txt = strcat("\leftarrow ", int2str(i));
      text(goal_pos_x(path_idx), goal_pos_z(path_idx), goal_pos_y(path_idx), txt, 'color', 'blue');
    end
    if ee_pos_x(path_idx) ~= 9999.0
      plot3(ee_pos_x(path_idx), ee_pos_z(path_idx), goal_pos_z(path_idx), 'o', 'MarkerFaceColor', 'magenta', 'color', 'magenta')
      txt = strcat("\leftarrow ", int2str(i));
      %text(ee_pos_x(path_idx), ee_pos_z(path_idx), goal_pos_z(path_idx), txt, 'color', 'magenta');
    end
    i = i + 1;
  end
  last_trans_idx = trans_idx;
  
  if endgoal_pos_x(path_idx) ~= 9999.0
      plot3(endgoal_pos_x(path_idx), goal_pos_z(path_idx), endgoal_pos_y(path_idx), 'o', 'MarkerFaceColor', 'red', 'color', 'red')
  end
  
  % Figure settings
  % have to be re-applied, otherwise it applies only on the first figure
  set(gca, 'FontSize', 26);
  set(gcf, 'Position', get(0, 'Screensize'));
  
  close all;
end

end