function mre_global_figure(path)

if nargin < 1
  %path = "../results_finished/";
  %path = "../results_jan2021_test_maxmodifs/";
  %path = "../results_feb2021_aft_review/";
  %path = "../results_feb2021_aft_review_3Deefix/";
  path = "../results_final_tcds";
end

init_path = pwd;
%% Init
% List of set of exps folders
cd(path); % change dir
folders_struct = dir; % get list of set of exps folders
folders_names_cells = {folders_struct.name}; % get cell array of names
folders = string(folders_names_cells(3:end)); % into string array, ignore . and ..
% only take folders that start with 'exp'
cleaned_folders_idx = regexp(folders, regexptranslate('wildcard', 'exp*'));
cleaned_folder_idx_bin = zeros(1, size(cleaned_folders_idx, 2));
for i = 1:1:size(cleaned_folders_idx, 2)
  cleaned_folder_idx_bin(i) = cleaned_folders_idx{i};
end
folders = folders(cleaned_folder_idx_bin == 1);

% Plots parameters 
data_range = 100:100:1000; % test checkpoints (default: every 100 iterations)
co = get(gca,'ColorOrder'); % Initial colors
newColors = [co; 0 0 0; 1 0 0; 1 1 0; 1 0 1;]; % Add new colors for more functions on the same plot

%% Processing

% row = 1:6 lowres torso, 7:12 lowres head, 13:16 highres torso, 17:20 highres head
exp_mean_plots = zeros(size(folders, 2), size(data_range, 2));
std_mean_plots = zeros(size(folders, 2), size(data_range, 2));
fold_idx = 1;

% For each set of experiment
for fold = folders
  % Figure settings
  % have to be re-applied, otherwise it applies only on the first figure
  set(gca, 'FontSize', 35);
  set(gcf, 'Position', get(0, 'Screensize'));
  % This sets the new colour scheme and automatically opens a figure with it.
  set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
  
  % Figure labels and settings
  grid on
  xlabel('Iteration number')
  ylabel('Mean reaching distance [cm]')
  %title('Comparison of same-exp results')
 
  cd(fold);
  exp_folds_struct = dir;  % get list of individual exps folders
  exp_names_cells = {exp_folds_struct.name}; % get cell array of names
  exps = string(exp_names_cells(3:end)); % into string array, ignore . and ..
  % only take folders that start with 'exp'
  cleaned_exps_idx = regexp(exps, regexptranslate('wildcard', 'exp*'));
  cleaned_exps_idx_bin = zeros(1, size(cleaned_exps_idx, 2));
  for i = 1:1:size(cleaned_exps_idx, 2)
    cleaned_exps_idx_bin(i) = cleaned_exps_idx{i};
  end
  exps = exps(cleaned_exps_idx_bin == 1);
  
  all_exps_mean = rand(size(exps, 2), size(data_range, 2));
  exp_idx = 1;
  
  % For each individual experiment, get the results and plot them
  for exp = exps
    mean_errs = zeros(size(data_range));
    datarange_idx = 1;
    stds = zeros(size(data_range));
    skin_portion = zeros(size(data_range));
    
    
    % For each test checkpoint (default: every 100 iterations)
    for i = data_range
      filename = strcat(exp, '/output/data-', string(i), '.txt');
      fileID = fopen(filename,'r');
      data = fscanf(fileID, '%f');
      data = reshape(data, [6, numel(data) / 6])';
      data_subset = data(data(:,5) ~= 9999, 5) .* 100;
      
      reaching_mean_err = mean(data_subset);
      reaching_std = std(data_subset);
      reaching_skin_portion = numel(data_subset) / numel(data(:,5));
      
      mean_errs(datarange_idx) = reaching_mean_err;
      stds(datarange_idx) = reaching_std;
      skin_portion(datarange_idx) = reaching_skin_portion;
      
      datarange_idx = datarange_idx + 1;
      fclose(fileID);
    end
    plot(data_range, mean_errs, '--', 'LineWidth', 3);
    hold on;
    all_exps_mean(exp_idx, 1:end) = mean_errs;
    exp_idx = exp_idx + 1;
  end
  final_means = nanmean(all_exps_mean);
  final_std = std(all_exps_mean);
  plot(data_range, final_means, 'LineWidth', 4);
  errorbar(data_range, final_means, final_std, 'Color', [1 0 1], 'LineWidth', 1.5);
  
  % Figure legend and settings
  %legend(labels, 'FontSize', 31, 'Location','NorthWest');
  axis([data_range(1) data_range(end)  0 15]);
  yticks([0:2:12]);
  %xticks([100 300 500 800 1000]);
  
  % Save figure
  fig_name = strcat('MRE_', fold, '_all');
  %saveas(gcf, fig_name, 'fig')
  %saveas(gcf, fig_name, 'epsc')
  
  exp_mean_plots(fold_idx, 1:end) = final_means;
  std_mean_plots(fold_idx, 1:end) = final_std;
  fold_idx = fold_idx + 1;
  
  cd ..;
  close all;
end

cd("../TCDS_results");
% row = 1:6 highres torso, 7:11 highres head
%% Plot final torso highres
% Figure settings
set(gca, 'FontSize', 42);
set(gcf, 'Position', get(0, 'Screensize'));
set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
labels = ["RMB" "RGB" "DGB" "DGB-nr", "DMB", "PBCGB"];
%labels = ["ac-fa" "ac-kino" "ac-rcPfa" "ac-r", "ac-ur", "ac-vs", "ac-vsPfaPrc-dart", "ac-vsPfaPrc", "bc-kino", "bc-ur"];

plot(data_range, exp_mean_plots(1:6, 1:end), 'LineWidth', 5);
%errorbar(data_range, exp_mean_plots(1:6, 1:end), std_mean_plots(1:6, 1:end), 'Color', [1 0 1], 'LineWidth', 1.5);
grid on
xlabel('Iteration number')
ylabel('Mean reaching error [cm]')
%title('Comparison of exploration strategies, torso & right hand - Low res skin')
legend(labels, 'FontSize', 60, 'Location','NorthWest');
axis([100 1000  0 inf]);

fig_name = strcat('MRE_highres_torso_legend');
saveas(gcf, fig_name, 'fig')
saveas(gcf, fig_name, 'epsc')

%% Plot final head highres
% Figure settings
set(gca, 'FontSize', 42);
set(gcf, 'Position', get(0, 'Screensize'));
set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
labels = ["DGB" "DGB-nohj" "DGB-nr" "PBCGB" "PBCGB-incgs"];
%labels = ["DGB" "DGB-noheadjoints" "PBCGB"];


plot(data_range, exp_mean_plots(7:11, 1:end), 'LineWidth', 5);
%errorbar(data_range, exp_mean_plots(7:9, 1:end), std_mean_plots(7:9, 1:end), 'Color', [1 0 1], 'LineWidth', 1.5);
grid on
xlabel('Iteration')
ylabel('Mean reaching error [cm]')
%title('Comparison of exploration strategies, torso & right hand - Low res skin')
legend(labels, 'FontSize', 60, 'Location','NorthWest');
axis([100 1000  0 inf]);

fig_name = strcat('MRE_highres_head_legend');
saveas(gcf, fig_name, 'fig')
saveas(gcf, fig_name, 'epsc')

% %% Plot final head highres -- noJoints
% % Figure settings
% set(gca, 'FontSize', 29);
% set(gcf, 'Position', get(0, 'Screensize'));
% set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
% labels = ["RMB" "RGB" "DGB 15x15" "DGB 32x32"];
% 
% 
% plot(data_range, exp_mean_plots(25:28, 1:end), 'LineWidth', 3);
% grid on
% xlabel('Iteration')
% ylabel('Mean reaching distance [cm]')
% %title('Comparison of exploration strategies, torso & right hand - Low res skin')
% legend(labels, 'FontSize', 31, 'Location','NorthWest');
% axis([100 1000  0 inf]);
% 
% fig_name = strcat('MRE_highres_head_noJoints');
% saveas(gcf, fig_name, 'fig')
% saveas(gcf, fig_name, 'epsc')

%% end, cleaning and reseting path
% For convenience, go back to the scripts folder
close all;
cd(init_path);
clear;
end