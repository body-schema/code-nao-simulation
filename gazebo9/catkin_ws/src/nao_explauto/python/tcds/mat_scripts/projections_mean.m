function projections_mean(path, skin_id)

if nargin < 2
  skin_id = 1;
  if nargin < 2
    %path = "../results_finished/";
    %path = "../results_jan2021_test_maxmodifs/";
    %path = "../results_feb2021_aft_review/";
    %path = "../results_feb2021_aft_review_3Deefix/";
    %path = "../results_feb2021_aft_review_3Deefix_Compromised";
    path = "../results_final_tcds";
  end
end

%% Init
data_range = 1000; % test checkpoints (default: every 100 iterations)
%data_range = 15000; % test checkpoints (default: every 1000 iterations)
script_path = pwd; % save script path, to call it later
cd(path); % change dir to the one containing the experiments

% highres torso
if skin_id == 1
  folders = ["exp0401", "exp0403" 'exp0405', 'exp0405-nr', 'exp0410'];
  %folders = ["exp0411"];
  %folders = ["exp0405-dgb-planar-htorso-rhand_ac_fa_ur", "exp0405-dgb-planar-htorso-rhand_ac_kinova" 'exp0405-dgb-planar-htorso-rhand_ac_rcPfa_ur', 'exp0405-dgb-planar-htorso-rhand_ac_rc_ur', 'exp0405-dgb-planar-htorso-rhand_ac_ur', "exp0405-dgb-planar-htorso-rhand_ac_vc_kinova", "exp0405-dgb-planar-htorso-rhand_ac_vsPfaPrc_dart_kinova" 'exp0405-dgb-planar-htorso-rhand_ac_vsPfaPrc_kinova', 'exp0405-dgb-planar-htorso-rhand_bc_kinova', 'exp0405-dgb-planar-htorso-rhand_bc_ur', "exp0405-dgb-planar-htorso-rhand_ac_vsPfaPrc_1sdelay_ur", "exp0405-dgb-planar-htorso-rhand_ac_vsPfaPrc_2sdelay_kinova"];
  %folders = ["exp0411-pbgb-planar-htorso-rhand_ac_vsPfaPrc_kinova", "exp0411-pbgb-planar-htorso-rhand_all"];
  skin_filename = '../coordinate-maps/highres-torso-planar.txt';
% highres head
elseif skin_id == 2
  folders = ["exp0505", "exp0505-nr", "exp0505-nohj"];
  %folders = ["exp0511", "exp0511-incgs"];
  %folders = ["exp0511"];
  %folders = ["exp0505-noreset"];
  skin_filename = '../coordinate-maps/highres-head-planar.txt';
% highres head no head joints
% else % if skin_id == 3, in particular, but covers other errors/mistype/abuse
%   folders = ["exp061", "exp063", "exp064", "exp065", "exp069", "exp0610"];
%   folders = ["exp0611"];
%   skin_filename = '../coordinate-maps/highres-head.txt';
end

% Plots parameters 
colorMap = jet(256); % Create colormap

% Constants
ERROR_SCALING = 1; % Divides the reaching error by this value

% Get skin coordinates
fileID = fopen(skin_filename,'r');
skin = fscanf(fileID, '%f');
skin = reshape(skin, [2, numel(skin) / 2])';
fclose(fileID);

% Prepare axis limits from skin coordinates values
x_min = min(skin(:,1));
x_max = max(skin(:,1));
y_min = min(skin(:,2));
y_max = max(skin(:,2));
dx = x_max - x_min;
dy = y_max - y_min;

%% Processing
% For each set of experiment
for fold = folders
  cd(fold);
  exp_folds_struct = dir;  % get list of individual exps folders
  exp_names_cells = {exp_folds_struct.name}; % get cell array of names
  % into string array, ignore . and .. and figures created
  exps = string(exp_names_cells(3:end));
  % only take folders that start with 'exp'
  cleaned_exps_idx = regexp(exps, regexptranslate('wildcard', 'exp*'));
  cleaned_exps_idx_bin = zeros(1, size(cleaned_exps_idx, 2));
  for i = 1:1:size(cleaned_exps_idx, 2)
    cleaned_exps_idx_bin(i) = cleaned_exps_idx{i};
  end
  exps = exps(cleaned_exps_idx_bin == 1);
  all_exps_errors = {};
  all_exps_goals = [];
  exp_idx = 1;
  
  % For each individual experiment, get the results and plot them
  for exp = exps
    cd(exp);
    
    % Init/reset variables
    % control variables, set to 1 when the corresponding item has been plotted once
    % Goals, Skin taxels, reached_taxels, unreached_taxels, reached_with_errors
    ctrl_handles = [0, 0, 0, 0, 0];
    handles = zeros(5); % Contains plot handles
    
    % Figure settings
    % have to be re-applied, otherwise it applies only on the first figure
    set(gca, 'FontSize', 40);
    set(gcf, 'Position', get(0, 'Screensize'));
    ay = gca;
    ay.YAxis.TickLabelFormat = '%.2f';
    ay.XAxis.TickLabelFormat = '%.2f';
    grid on;
    hold on;
    %title('Distribution of goals in Direct Optimization Discrete Goal Babbling, 32x32')
    
    
    % Check if goals exists, if yes, plot them
    if exist('./output/states.txt', 'file') == 2
      % Get goals
      goals_filename = './output/states.txt';
      fileID = fopen(goals_filename, 'r');
      states = fscanf(fileID, '%f');
      states = reshape(states, [9, numel(states) / 9])';
      goals = states(:, 2:3);
      last_goal = min(size(goals, 1), data_range);
      goals = goals(goals(1:last_goal, 1) ~= 9999.0, :);
      all_exps_goals = [all_exps_goals; goals];
      fclose(fileID);
      
      % Plot while keeping handle + set flag
      handles(1) = scatter(goals(:,1),goals(:,2), [], [.7,.7,.7], 'filled');
      ctrl_handles(1) = 1;
    end
    
    % Check if it's high-res skin, in which case draw all skin taxels
    if contains(skin_filename, "highres")
      % We already have the data in the skin variable, we just plot them with
      % the handle and setting the flag
      handles(2) = scatter(skin(:,1),skin(:,2), 'black', 'filled');
      ctrl_handles(2) = 1;
    end
    
    % Get reaching errors
    filename = strcat('./output/data-', string(data_range), '.txt');
    %filename = './output/data-1000.txt';
    fileID = fopen(filename,'r');
    errors = fscanf(fileID, '%f');
    errors = reshape(errors, [6, numel(errors) / 6])';
    all_exps_errors = [all_exps_errors; errors];
    fclose(fileID);
    for i = 1:size(errors, 1)
      if errors(i, 5) == 0.0
        viscircles([errors(i, 1) errors(i, 2)], 0.003, 'Color', 'blue');
      elseif errors(i, 5) < 9999
        viscircles([errors(i, 1) errors(i, 2)],abs(errors(i, 5)/ERROR_SCALING), 'Color', 'magenta');
      else
        viscircles([errors(i, 1) errors(i, 2)], 0.003, 'Color', 'red');
      end
    end
    % Plot the dots - has to be done after circles are finished being drawn, as
    % otherwise they can overlap and hide the taxels dots
    for i = 1:size(errors, 1)
      if errors(i, 5) == 0.0
        handles(3) = scatter(errors(i,1), errors(i, 2), 'blue', 'filled');
        ctrl_handles(3) = 1;
      elseif errors(i, 5) < 9999
        handles(5) = scatter(errors(i,1), errors(i, 2), 'magenta', 'filled');
        ctrl_handles(5) = 1;
      else
        handles(4) = scatter(errors(i,1), errors(i,2), 'red', 'filled');
        ctrl_handles(4) = 1;
      end
    end
    
    % Set legend
    leg_idx = 1;
    % if we don't use (1,nnz), will create (nnz x nnz) matrix
    legends = strings(1, nnz(ctrl_handles)); % number of non-zero elements
    if ctrl_handles(1) == 1
      legends(leg_idx) = "Goals";
      leg_idx = leg_idx + 1;
    end
    if ctrl_handles(2) == 1
      legends(leg_idx) = "Untested taxels";
      leg_idx = leg_idx + 1;
    end
    if ctrl_handles(3) == 1
      legends(leg_idx) = "Reached taxels";
      leg_idx = leg_idx + 1;
    end
    if ctrl_handles(4) == 1
      legends(leg_idx) = "Unreached";
      leg_idx = leg_idx + 1;
    end
    if ctrl_handles(5) == 1
      if ERROR_SCALING ~= 1
        leg_err_scaled = strcat("Reached with error", '/', num2str(ERROR_SCALING));
        legends(leg_idx) = leg_err_scaled;
      else
        legends(leg_idx) = "Reached with error";
      end
    end
    handles(handles==0) = [];
    %legend(handles, legends, 'FontSize', 33, 'Location','NorthEast');
    
    xlim([x_min-dx x_max+dx]);
    ylim([y_min-dy y_max+dy]);
    axis equal
    
    % Save figure
    fig_name = strcat('Projection_', exp, '_cb');
    %saveas(gcf, fig_name, 'fig')
    %saveas(gcf, fig_name, 'epsc')
    
    exp_idx = exp_idx + 1;
    cd ..;
    close all;
  end
    
  % Figure settings for mean projections of each set of exp
  % have to be re-applied, otherwise it applies only on the first figure
  set(gca, 'FontSize', 50);
  set(gcf, 'Position', get(0, 'Screensize'));
  ay = gca;
  ay.YAxis.TickLabelFormat = '%.2f';
  ay.XAxis.TickLabelFormat = '%.2f';
  grid on;
  hold on;
  %title('Distribution of goals in Direct Optimization Discrete Goal Babbling, 32x32')
  
  % init some variables
  handles_mean = zeros(5); % Contains plot handles
  ctrl_handles_mean = [0, 0, 0, 0, 0];
  exp_idx = exp_idx - 1;
  nb_test_taxels = size(all_exps_errors{1}, 1);
  mean_errors = rand(size(all_exps_errors{1}, 1), 1);
  taxel_errors = rand(size(all_exps_errors{1}, 1), exp_idx+2);
  
  % Check if goals exists, if yes, plot them
  if ~isempty(all_exps_goals)      
    % Plot while keeping handle + set flag
    sampled_goals = datasample(all_exps_goals, round(size(all_exps_goals, 1)/(exp_idx*(data_range/1000))));
    
    handles_mean(1) = scatter(sampled_goals(:,1),sampled_goals(:,2), 100, [.7,.7,.7], 'filled');
    ctrl_handles_mean(1) = 1;
  end
  
  % Check if it's high-res skin, in which case draw all skin taxels
  if contains(skin_filename, "highres")
    % We already have the data in the skin variable, we just plot them with
    % the handle and setting the flag
    handles_mean(2) = scatter(skin(:,1),skin(:,2), 150, 'black', 'filled');
    ctrl_handles_mean(2) = 1;
  end
   
  % Calculate the mean error value for each taxel (coordinate.
  % if the taxel was completely unreached (no error) more than 5 times,
  % consider it as unreached
   
  % All coordinates are on the same order over all exps, so we only need to fix
  % them once, on the first two columns
  taxel_errors(:, 1) = all_exps_errors{1}(:, 1);
  taxel_errors(:, 2) = all_exps_errors{1}(:, 2);
  for i = 1:exp_idx
    taxel_errors(:, i+2) = all_exps_errors{i}(:, 5);
  end
  % For each taxel, we calculate the mean of its reaching errors, with a
  % threshold of minimum 5 errors to avoid taxels that are almost never reached
  % from having a mean calculated from few values
  for i = 1:nb_test_taxels
    logical_reached = taxel_errors(i,:) ~= 9999;
    logical_reached(1) = 0;
    logical_reached(2) = 0;
    not_unreached_taxels = taxel_errors(i, logical_reached);
    % Modif for the few exps that are not made 10 times
    threshold = floor(0.6*exp_idx);
    if size(not_unreached_taxels, 2) >= threshold
      mean_errors(i) = mean(not_unreached_taxels);
    else
      mean_errors(i) = 9999;
    end
  end
  % Plot the error circles
  for i = 1:size(mean_errors, 1)
    if abs(mean_errors(i)) < 0.003
      viscircles([taxel_errors(i, 1) taxel_errors(i, 2)], 0.003, 'Color', 'blue', 'LineWidth', 5);
    elseif mean_errors(i) < 9999
      viscircles([taxel_errors(i, 1) taxel_errors(i, 2)],abs(mean_errors(i)/ERROR_SCALING), 'Color', 'magenta', 'LineWidth', 5);
    else
      viscircles([taxel_errors(i, 1) taxel_errors(i, 2)], 0.003, 'Color', 'red', 'LineWidth', 5);
    end
  end
  % Plot the dots - has to be done after circles are finished being drawn, as
  % otherwise they can overlap and hide the taxels dots
  for i = 1:size(mean_errors, 1)
    if abs(mean_errors(i)) < 0.003
      handles_mean(3) = scatter(taxel_errors(i,1), taxel_errors(i, 2), 350, 'blue', 'filled');
      ctrl_handles_mean(3) = 1;
    elseif mean_errors(i) < 9999
      handles_mean(5) = scatter(taxel_errors(i,1), taxel_errors(i, 2), 350, 'magenta', 'filled');
      ctrl_handles_mean(5) = 1;
    else
      handles_mean(4) = scatter(taxel_errors(i,1), taxel_errors(i,2), 350, 'red', 'filled');
      ctrl_handles_mean(4) = 1;
    end
  end
  
  % Set legend
  leg_idx = 1;
  % if we don't use (1,nnz), will create (nnz x nnz) matrix
  legends = strings(1, nnz(ctrl_handles_mean)); % number of non-zero elements
  if ctrl_handles_mean(1) == 1
    legends(leg_idx) = "Goals";
    leg_idx = leg_idx + 1;
  end
  if ctrl_handles_mean(2) == 1
    legends(leg_idx) = "Untested taxels";
    leg_idx = leg_idx + 1;
  end
  if ctrl_handles_mean(3) == 1
    legends(leg_idx) = "Reached taxels";
    leg_idx = leg_idx + 1;
  end
  if ctrl_handles_mean(4) == 1
    legends(leg_idx) = "Unreached";
    leg_idx = leg_idx + 1;
  end
  if ctrl_handles_mean(5) == 1
    if ERROR_SCALING ~= 1
      leg_err_scaled = strcat("Reached with error", '/', num2str(ERROR_SCALING));
      legends(leg_idx) = leg_err_scaled;
    else
      legends(leg_idx) = "Reached with error";
    end
  end
  handles_mean(handles_mean==0) = [];
  %legend(handles_mean, legends, 'FontSize', 33, 'Location','NorthEast');
  
  % highres torso
  if skin_id == 1
    xlim([-0.10 0.10]);
    ylim([-0.01 0.08]);
  % highres head
  elseif skin_id == 2
    xlim([-0.08 0.08]);
    ylim([-0.01 0.12]);
  end
  
  %xlim([x_min-dx x_max+dx]);
  %ylim([y_min-dy y_max+dy]);
  axis equal 
  
  
  % Save figure
  fig_name = strcat('MeanProjection_', fold, '_', num2str(data_range), '_', num2str(ERROR_SCALING), '_2D');
  %fig_name = strcat('MeanProjection_', fold, 'unscaled');
  saveas(gcf, fig_name, 'fig')
  saveas(gcf, fig_name, 'epsc')
    
  cd ..;
  close all;
end

cd(script_path);
% if skin_id > 0 && skin_id < 6
%   projections_exps_mean(path, skin_id+1);
% end
clear;
end