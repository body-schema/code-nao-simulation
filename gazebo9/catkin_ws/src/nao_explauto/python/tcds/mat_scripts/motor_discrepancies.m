function motor_discrepancies(path, skin_filename, outcomes, exp_name)

nb_joints = 5;
joints_dist = zeros(1, nb_joints);
joints_dist(1) = abs(0.0349066 - 1.54462); % elbow roll
joints_dist(2) = abs(-2.08567 - 2.08567); % Elbow Yaw
joints_dist(3) = abs(-2.08567 - 2.08567); % Shoulder pitch
joints_dist(4) = abs(-1.32645 - 0.314159); % Shoulder Roll
joints_dist(5) = abs(-1.82387 - 1.82387); % Wrist Yaw

if contains(skin_filename, "head") && ~contains(exp_name, "noheadjoints") 
  nb_joints = 7;
  joints_dist = [joints_dist abs(-2.08567 - 2.08567)]; % Head yaw
  joints_dist = [joints_dist abs(-0.671952 - 0.514872)]; % Head pitch
end

joints_dist = joints_dist .* 0.10; % max range to not discard = 10% of the range size

outcomes = cell2mat(outcomes(1));
with_outcomes = transpose(find(outcomes ~= 9999.0));
without_outcomes = transpose(find(outcomes == 9999.0));

cd(path);
mkdir("./motor_discrepancies");
filename = './output/motors.txt';
file_targets = fopen(filename,'r');
targets = fscanf(file_targets, '%f');
targets = reshape(targets, [nb_joints, numel(targets) / nb_joints])';
fclose(file_targets);

filename = './output/act_joints.txt';
file_actuals = fopen(filename,'r');
actuals = fscanf(file_actuals, '%f');
actuals = reshape(actuals, [nb_joints, numel(actuals) / nb_joints])';
fclose(file_actuals);

diffs_with_outcomes = targets(with_outcomes, :) - actuals(with_outcomes, :);
diffs_without_outcomes = targets(without_outcomes, :) - actuals(without_outcomes, :);

diffs = targets - actuals;

max_values = max(diffs);
min_values = min(diffs);
median_values = median(diffs);
mean_values = mean(diffs);
std_values = std(diffs);

woutcome_max_values = max(diffs_with_outcomes);
woutcome_min_values = min(diffs_with_outcomes);
woutcome_median_values = median(diffs_with_outcomes);
woutcome_mean_values = mean(diffs_with_outcomes);
woutcome_std_values = std(diffs_with_outcomes);

nooutcome_max_values = max(diffs_without_outcomes);
nooutcome_min_values = min(diffs_without_outcomes);
nooutcome_median_values = median(diffs_without_outcomes);
nooutcome_mean_values = mean(diffs_without_outcomes);
nooutcome_std_values = std(diffs_without_outcomes);

% Plot Joint Error Range histogram for each joint
for i = 1:1:nb_joints
  histogram(diffs(:, i));
  xline(joints_dist(i),'--r', 'LineWidth', 2);
  xline(-joints_dist(i),'--r', 'LineWidth', 2);
  fig_name = strcat("./motor_discrepancies/JER_all_", int2str(i));
  saveas(gcf, fig_name, 'fig')
  saveas(gcf, fig_name, 'epsc')
  close all;
end

% Plot Joint Error Range histogram for each joint - only commands with outcomes
for i = 1:1:nb_joints
  histogram(diffs_with_outcomes(:, i));
  xline(joints_dist(i),'--r', 'LineWidth', 2);
  xline(-joints_dist(i),'--r', 'LineWidth', 2);
  fig_name = strcat("./motor_discrepancies/JER_with_outcome_", int2str(i));
  saveas(gcf, fig_name, 'fig')
  saveas(gcf, fig_name, 'epsc')
  close all;
end

% Plot Joint Error Range histogram for each joint - only commands with outcomes
for i = 1:1:nb_joints
  histogram(diffs_without_outcomes(:, i));
  xline(joints_dist(i),'--r', 'LineWidth', 2);
  xline(-joints_dist(i),'--r', 'LineWidth', 2);
  fig_name = strcat("./motor_discrepancies/JER_without_outcome_", int2str(i));
  saveas(gcf, fig_name, 'fig')
  saveas(gcf, fig_name, 'epsc')
  close all;
end

end