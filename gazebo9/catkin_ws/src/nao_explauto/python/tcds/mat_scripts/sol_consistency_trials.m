function sol_consistency_trials(path, skin_id)

if nargin < 2
  skin_id = 1;
  if nargin < 1
    path = "../results_stats/";
  end
end

% highres torso
if skin_id == 1
  folders = ["exp0405", "exp0405-noReset", "exp0411_ignore"];
  skin_filename = '../coordinate-maps/highres-torso-planar.txt';
  nb_joints = 5;
% highres head
elseif skin_id == 2
  folders = ["exp0505", "exp0505-noReset", "exp0511_ignore"];
  skin_filename = '../coordinate-maps/highres-head-planar.txt';
  nb_joints = 7;
% highres head no head joints
% else % if skin_id == 3, in particular, but covers other errors/mistype/abuse
%   folders = ["exp061", "exp063", "exp064", "exp065", "exp069", "exp0610"];
%   folders = ["exp0611"];
%   skin_filename = '../coordinate-maps/highres-head.txt';
end

%% Init
script_path = pwd; % save script path, to call it later
cd(path); % change dir to the one containing the experiments

for fold = folders
  cd(fold);
  exp_folds_struct = dir;  % get list of individual exps folders
  exp_names_cells = {exp_folds_struct.name}; % get cell array of names
  exps = string(exp_names_cells(3:end)); % into string array, ignore . and ..
  % only take folders that start with 'exp'
  cleaned_exps_idx = regexp(exps, regexptranslate('wildcard', 'exp*'));
  cleaned_exps_idx_bin = zeros(1, size(cleaned_exps_idx, 2));
  for i = 1:1:size(cleaned_exps_idx, 2)
    cleaned_exps_idx_bin(i) = cleaned_exps_idx{i};
  end
  exps = exps(cleaned_exps_idx_bin == 1);
  
  i = 1;
  for exp = exps
    data_filename = strcat(exp, '/output/data-1000.txt');
    fileID = fopen(data_filename,'r');
    data = fscanf(fileID, '%f');
    data = reshape(data, [6, numel(data) / 6])';
    
    mactual_filename = strcat(exp, '/output/data-mactual-1000.txt');
    fileID = fopen(mactual_filename,'r');
    mactual = fscanf(fileID, '%f');
    mactual = reshape(mactual, [nb_joints, numel(mactual) / nb_joints])';
    
    mtarget_filename = strcat(exp, '/output/data-mtarget-1000.txt');
    fileID = fopen(mtarget_filename,'r');
    mtarget = fscanf(fileID, '%f');
    mtarget = reshape(mtarget, [nb_joints, numel(mtarget) / nb_joints])';
    
    if i == 1
      datas = data;
      mactuals = mactual;
      mtargets = mtarget;
    else
      datas = [datas; data];
      mactuals = [mactuals; mactual];
      mtargets = [mtargets; mtarget];
    end
    
    i = i + 1;
  end
  a = 0;
end  

cd(script_path);
% if skin_id > 0 && skin_id < 3
%   sol_consistency_trials(path, skin_id+1);
% end
clear all;
end