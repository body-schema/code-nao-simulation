function nb_discarded = count_discarded_cmd(path)

cd(path);

filename = './output/nb_discarded_cmd.txt';
file_discarded = fopen(filename,'r');
nb_discarded = fscanf(file_discarded, '%f');
nb_discarded = reshape(nb_discarded, [1, numel(nb_discarded) / 1])';
fclose(file_discarded);

end