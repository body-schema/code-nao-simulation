% Get skin coordinates
formatSpec = '%f';
skin_filename = 'skin-coordinates/torso-highres.txt';
fileID = fopen(skin_filename,'r');
skin = fscanf(fileID, formatSpec);
skin = reshape(skin, [3, numel(skin) / 3])';
fclose(fileID);

% New planar projection:
new_skin = skin(:,2:3);
new_skin = num2str(new_skin, "%.18f %.18f");
new_skin_filename = 'skin-coordinates/torso-highres-plannar.txt';
fileID = fopen(new_skin_filename,'w');
fprintf(fileID, "%s\n", new_skin);
fclose(fileID);

print('test');