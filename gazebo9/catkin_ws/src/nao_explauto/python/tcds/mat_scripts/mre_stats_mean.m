function mre_stats_mean(path)

if nargin < 1
  %path = "../results_finished/";
  %path = "../results_jan2021_test_maxmodifs/";
  %path = "../results_feb2021_aft_review/less1kit/";
  %path = "../results_feb2021_aft_review_3Deefix/more1kit/";
  path = "../results_final_tcds/less1kit/";
end

init_path = pwd;
%% Init
% List of set of exps folders
cd(path); % change dir
folders_struct = dir; % get list of set of exps folders
folders_names_cells = {folders_struct.name}; % get cell array of names
folders = string(folders_names_cells(3:end)); % into string array, ignore . and ..
% only take folders that start with 'exp'
cleaned_folders_idx = regexp(folders, regexptranslate('wildcard', 'exp*'));
cleaned_folder_idx_bin = zeros(1, size(cleaned_folders_idx, 2));
for i = 1:1:size(cleaned_folders_idx, 2)
  cleaned_folder_idx_bin(i) = cleaned_folders_idx{i};
end
folders = folders(cleaned_folder_idx_bin == 1);

% Plots parameters 
data_range = 100:100:1000; % test checkpoints (default: every 100 iterations)
co = get(gca,'ColorOrder'); % Initial colors
newColors = [co; 0 0 0; 1 0 0; 1 1 0; 1 0 1;]; % Add new colors for more functions on the same plot

%% Processing
% For each set of experiment
for fold = folders
  % Figure settings
  % have to be re-applied, otherwise it applies only on the first figure
  set(gca, 'FontSize', 29);
  set(gcf, 'Position', get(0, 'Screensize'));
  % This sets the new colour scheme and automatically opens a figure with it.
  set(gca, 'ColorOrder', newColors, 'NextPlot', 'replacechildren');
  
  % Figure labels and settings
  grid on
  xlabel('Iteration number')
  ylabel('Mean reaching distance [cm]')
  %title('Comparison of same-exp results')
 
  cd(fold);
  exp_folds_struct = dir;  % get list of individual exps folders
  exp_names_cells = {exp_folds_struct.name}; % get cell array of names
  exps = string(exp_names_cells(3:end)); % into string array, ignore . and ..
  % only take folders that start with 'exp'
  cleaned_exps_idx = regexp(exps, regexptranslate('wildcard', 'exp*'));
  cleaned_exps_idx_bin = zeros(1, size(cleaned_exps_idx, 2));
  for i = 1:1:size(cleaned_exps_idx, 2)
    cleaned_exps_idx_bin(i) = cleaned_exps_idx{i};
  end
  exps = exps(cleaned_exps_idx_bin == 1);
  
  all_exps_mean = rand(size(exps, 2), size(data_range, 2));
  exp_idx = 1;
  
  % For each individual experiment, get the results and plot them
  for exp = exps
    mean_errs = zeros(size(data_range));
    datarange_idx = 1;
    stds = zeros(size(data_range));
    skin_portion = zeros(size(data_range));
    
    
    % For each test checkpoint (default: every 100 iterations)
    for i = data_range
      filename = strcat(exp, '/output/data-', string(i), '.txt');
      fileID = fopen(filename,'r');
      data = fscanf(fileID, '%f');
      data = reshape(data, [6, numel(data) / 6])';
      data_subset = data(data(:,5) ~= 9999, 5) .* 100;
      
      reaching_mean_err = mean(data_subset);
      reaching_std = std(data_subset);
      reaching_skin_portion = numel(data_subset) / numel(data(:,5));
      
      mean_errs(datarange_idx) = reaching_mean_err;
      stds(datarange_idx) = reaching_std;
      skin_portion(datarange_idx) = reaching_skin_portion;
      
      datarange_idx = datarange_idx + 1;
      fclose(fileID);
    end
    plot(data_range, mean_errs, '--', 'LineWidth', 3);
    hold on;
    all_exps_mean(exp_idx, 1:end) = mean_errs;
    exp_idx = exp_idx + 1;
  end
  final_means = nanmean(all_exps_mean);
  final_std = std(all_exps_mean);
  plot(data_range, final_means, 'LineWidth', 4);
  errorbar(data_range, final_means, final_std, 'Color', [1 0 1], 'LineWidth', 1.5);
  
  % Figure legend and settings
  %legend(labels, 'FontSize', 31, 'Location','NorthWest');
  axis([data_range(1) data_range(end)  0 15]);
  yticks([0:2:12]);
  %xticks([100 300 500 800 1000]);
  
  % Save figure
  fig_name = strcat('MRE_', fold, '_all');
  saveas(gcf, fig_name, 'fig')
  saveas(gcf, fig_name, 'epsc')
  
  cd ..;
  close all;
end
% For convenience, go back to the scripts folder
cd(init_path);
clear;
end