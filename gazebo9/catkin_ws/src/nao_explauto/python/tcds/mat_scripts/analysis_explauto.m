function analysis_explauto(path, skin_id)

if nargin < 2
  skin_id = 2;
  if nargin < 2
    path = "../results_final_tcds/";
  end
end

%% Init
script_path = pwd; % save script path, to call it later
cd(path); % change dir to the one containing the experiments

% highres torso
if skin_id == 1
  folders = ["exp0401", "exp0403" 'exp0405', 'exp0405-nr', 'exp0410'];
  folder = "exp0410";
  skin_filename = '../coordinate-maps/highres-torso-planar.txt';
% highres head
elseif skin_id == 2
  folders = ["exp0505", "exp0505-nr", "exp0505-nohj"];
  folder = "exp0505-nohj";
  skin_filename = '../coordinate-maps/highres-head-planar.txt';
end

% Plots parameters 
colorMap = jet(256); % Create colormap

% Get skin coordinates
file_skin = fopen(skin_filename,'r');
skin = fscanf(file_skin, '%f');
skin = reshape(skin, [2, numel(skin) / 2])';
fclose(file_skin);

% Prepare axis limits from skin coordinates values
x_min = min(skin(:,1));
x_max = max(skin(:,1));
y_min = min(skin(:,2));
y_max = max(skin(:,2));
dx = x_max - x_min;
dy = y_max - y_min;

% For set of experiment
cd(folder);
exp_folds_struct = dir;  % get list of individual exps folders
exp_names_cells = {exp_folds_struct.name}; % get cell array of names
exps = string(exp_names_cells(3:end)); % into string array, ignore . and ..
% only take folders that start with 'exp'
cleaned_exps_idx = regexp(exps, regexptranslate('wildcard', 'exp*'));
cleaned_exps_idx_bin = zeros(1, size(cleaned_exps_idx, 2));
for i = 1:1:size(cleaned_exps_idx, 2)
  cleaned_exps_idx_bin(i) = cleaned_exps_idx{i};
end
exps = exps(cleaned_exps_idx_bin == 1);

% For each individual experiment, get the results and plot them

nb_discarded = 0;
for exp = exps
  cd(exp);
  % Get all states
  filename = './output/states.txt';
  file_states = fopen(filename,'r');
  states = textscan(file_states,'%d %f %f %f %f %f %f %f %f');
  fclose(file_states);
  
  full_path = pwd;
  %Plot the differences between target and actual motor configuration
  %cd(script_path);
  %motor_discrepancies(full_path, skin_filename, states(7), folder);
  
  %Sum discarded commands to record/std them
  cd(script_path);
  nb_discarded = [nb_discarded; count_discarded_cmd(full_path)];
  
  %Plot the go to goal interm goals ee position
  %for each back and forth
  %cd(script_path);
  %plot_3D_goalsEE(full_path, states, skin, skin_filename);
  
  cd(full_path);
  cd ..;
end

%file = fopen("nb_discarded.txt",'w');
total_discarded = sum(nb_discarded);
mean_discarded = mean(nb_discarded);
std_discarded = std(nb_discarded);
text = 'total discarded: %d mean discarded: %f std: %f \n';
%fprintf(file, text, total_discarded, mean_discarded, std_discarded);
fprintf(text, total_discarded, mean_discarded, std_discarded);
%fclose(file);

cd(script_path);
% if skin_id > 0 && skin_id < 3
%   analysis_explauto(path, skin_id+1);
% end
clear;
end
