clc;
clear all;
close all;


% Plot skin
formatSpec = '%f';
skin_filename = '../coordinate-maps/highres-head-planar.txt';
fileID = fopen(skin_filename,'r');
skin = fscanf(fileID, formatSpec);
skin = reshape(skin, [2, numel(skin) / 2])';
fclose(fileID);

set(gca, 'FontSize', 14);
%set(gcf, 'Position', get(0, 'Screensize'));
%ay = gca;
%ay.YAxis.TickLabelFormat = '%.2f';
%ay.XAxis.TickLabelFormat = '%.2f';
xticks([-0.06 -0.03 0 0.03 0.06])
yticks([0 0.03 0.06 0.09 0.12])
grid on;
hold on;

scatter(skin(:,1),skin(:,2), 'black', 'filled');

% Grid edges
edges = [];

% Targets
targets = [];
% targets = [1:10];
% targets = [targets 11:20];
% targets = [targets 21:30];
% targets = [targets 31:40];
% targets = [targets 41:50];
% targets = [targets 51:60];
% targets = [targets 61:70];
% targets = [targets 71:80];
% targets = [targets 81:90];
% targets = [targets 91:100];
% targets = [targets 101:110];
% targets = [targets 111:120];
% targets = [targets 121:130];
% targets = [targets 131:140];
% targets = [targets 141:150];
% targets = [targets 151:160];
% targets = [targets 161:170];
% targets = [targets 171:180];
% targets = [targets 181:190];
% targets = [targets 191:200];
% targets = [targets 201:210];
% targets = [targets 211:220];
% targets = [targets 221:230];
% targets = [targets 231:240];

%scatter(skin(targets,1), skin(targets,2), 'red', 'filled');

%% Original
% targets = [2 34 43 51 57 59 74 101 112 126 154 165 171 177 179 194 227 236];
% 
% scatter(skin(targets,1), skin(targets,2), 'blue', 'filled');
% 
% edges = [
%     % Horizontal
%     2 34; 2 57; 57 59; 59 51; 34 43; 51 74; 74 101; 101 112; 59 179; 171 179;
%     177 179; 171 126; 177 194; 126 154; 154 165; 194 227; 227 236;
%     % Vertical
%     2 74; 51 57; 34 101; 43 112; 171 177; 126 194; 154 227; 165 236; 51 177;
%     74 194; 101 227; 112 236;
% ];


%% + 2 on each edges
% first tcds exps
%targets = [2 8 18 30 34 43 51 57 59 74 101 112 126 130 140 154 165 171 177 179 194 227 236];
% updated proposition 1 after review results
%targets = [2 8 18 30 34 43 56 59 62 74 100 101 112 126 130 140 148 154 165 172 179 186 194 218 227 236];
% updated proposition 2 after review results: the one we decided to use
targets = [9 19 26 36 42 59 74 88 94 109 112 129 139 142 152 166 179 194 210 214 229 236];

%below: retrieved taxels due to offset problem
%targets = [10 20 27 37 43 60 75 89 95 110 113 130 140 143 153 167 180 195 211 215 230 237];

scatter(skin(targets,1), skin(targets,2), 'blue', 'filled');

edges = [
    % Horizontal
    2 34; 2 57; 57 59; 59 51; 34 43; 51 74; 74 101; 101 112; 59 179; 171 179;
    177 179; 171 126; 177 194; 126 154; 154 165; 194 227; 227 236; 57 8; 8 18;
    18 34; 171 130; 130 126; 130 140; 140 154;
    % Vertical
    2 74; 51 57; 34 101; 43 112; 171 177; 126 194; 154 227; 165 236; 51 177;
    74 194; 101 227; 112 236; 8 2;
];

%for i = 1:size(edges,1)
%	plot([skin(edges(i,1),1) skin(edges(i,2),1)], [skin(edges(i,1),2) skin(edges(i,2),2)], 'Color', [0.7,.7,1]);
%end

% Plot targets once again (needed for proper legend)
scatter(skin(targets,1), skin(targets,2), 120, 'blue', 'filled');

%new_targets = [30 56 62 100 148 172 186 218];
%scatter(skin(new_targets,1), skin(new_targets,2), 80, 'red', 'filled');

legend('Skin taxels', 'Target goals', 'FontSize', 14) %, 'Location', 'southeast');

% title('Incidence graph')
xlabel('X [m]')
ylabel('Y [m]')
title('Target goals')

