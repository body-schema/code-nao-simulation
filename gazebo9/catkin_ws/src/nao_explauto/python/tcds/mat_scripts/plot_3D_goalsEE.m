function plot_3D_goalsEE(path, states, skin, skin_filename)

cd(path);

ee_pos_x = cell2mat(states(4));
ee_pos_y = cell2mat(states(5));
ee_pos_z = cell2mat(states(6));

goal_pos_x = cell2mat(states(2));
goal_pos_y = cell2mat(states(3));
nb_goals = size(goal_pos_x, 1);
goal_pos_z = zeros(nb_goals, 1);

% Plots parameters 
colorMap = jet(256); % Create colormap

for goal_idx = 1:1:nb_goals
  
  % Check if it's high-res skin, in which case draw all skin taxels
  if contains(skin_filename, "highres")
    % We already have the data in the skin variable, we just plot them with
    % the handle and setting the flag
    plot3(skin(:,1), zeros(size(skin(:,1))), skin(:,2), '.',  'color', 'black');
    hold on;
  end
  
  if goal_pos_x(goal_idx) ~= 9999.0
    plot3(goal_pos_x(goal_idx), goal_pos_z(goal_idx), goal_pos_y(goal_idx), 'o', 'MarkerFaceColor', 'blue', 'color', 'blue')
    txt = strcat("\leftarrow ", int2str(goal_idx));
    text(goal_pos_x(goal_idx), goal_pos_z(goal_idx), goal_pos_y(goal_idx), txt, 'color', 'blue');
  end
  if ee_pos_x(goal_idx) ~= 9999.0
    plot3(ee_pos_x(goal_idx), ee_pos_z(goal_idx), goal_pos_z(goal_idx), 'o', 'MarkerFaceColor', 'magenta', 'color', 'magenta')
    txt = strcat("\leftarrow ", int2str(goal_idx));
    text(ee_pos_x(goal_idx), ee_pos_z(goal_idx), goal_pos_z(goal_idx), txt, 'color', 'magenta');
  end
  
  % Figure settings
  % have to be re-applied, otherwise it applies only on the first figure
  set(gca, 'FontSize', 26);
  set(gcf, 'Position', get(0, 'Screensize'));
  
  close all;
end
end