function weight_ranges(path, weights, outcomes)

cd(path);
weights = cell2mat(weights(1));
outcomes = cell2mat(outcomes(1));

with_outcomes = transpose(find(outcomes ~= 9999.0));
weights_with_outcomes = weights(with_outcomes);

weight_zeros = transpose(find(weights_with_outcomes == 0.0));
nb_zeros = size(weight_zeros, 2);

max_value = max(weights_with_outcomes);
min_value = min(weights_with_outcomes);
median_value = median(weights_with_outcomes);
mean_value = mean(weights_with_outcomes);
std_value = std(weights_with_outcomes);

histogram(weights_with_outcomes);
fig_name = "weights_histogram";
saveas(gcf, fig_name, 'fig')
saveas(gcf, fig_name, 'epsc')
clear all;
close all;
end