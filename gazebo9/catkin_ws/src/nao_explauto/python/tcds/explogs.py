# General imports
import numpy as np
import pickle

class Explogs:

  def __init__(self):

    # Save once the experiment ended
    self.freached_taxels = 'output/reached_taxels.txt'
    self.fdiscarded_motor_cmd = 'output/nb_discarded_cmd.txt'

    # Save every itr
    self.factual_joints = 'output/act_joints.txt'
    self.fstates = 'output/states.txt'
    self.mat_mor_dat = 'output/mat_mor_dat.txt'
    self.fmotors = 'output/motors.txt'

    # Save every n itr
    self.fgb = 'models/gb-'
    self.fmodel = 'models/model-'
    self.ftouches = 'output/touches.txt'

    # Save during testing
    self.ftest_data = 'output/data-'
    self.ftest_end_effector = 'output/data-ee-'
    self.ftest_motor_actual = 'output/data-mactual-'
    self.ftest_motor_target = 'output/data-mtarget-'
    self.ftest_3Dcoord_taxel_target = 'output/data-3D-ttaxel'

  def saveEndExp(self, reached_taxels, nb_discarded_cmd):
    fh_discarded_motor_cmd = open(self.fdiscarded_motor_cmd, "w")
    fh_reached_taxels = open(self.freached_taxels, "w")

    fh_discarded_motor_cmd.write(str(nb_discarded_cmd) + '\n')
    fh_reached_taxels.write('\n'.join(map(str, reached_taxels)))

    fh_discarded_motor_cmd.close()
    fh_reached_taxels.close()

  def saveEveryItr(self, mactual, state_array, mtarget, mmd=None, class_name=None):
    fh_actual_joints = open(self.factual_joints, 'a+')
    fh_states = open(self.fstates, 'a+')
    fh_motors = open(self.fmotors, 'a+')
    
    fh_actual_joints.write(' '.join(map(str, mactual))+ '\n')
    fh_states.write(' '.join(map(str, state_array))+ '\n')
    fh_motors.write(' '.join(map(str, mtarget)) + '\n')

    fh_actual_joints.close()
    fh_states.close()
    fh_motors.close()

    if mmd is not None:
      fh_mat_mor_dat = open(self.mat_mor_dat, 'a+')
      fh_mat_mor_dat.write(' '.join(map(str, mmd)) + ' ' + class_name + '\n') 
      fh_mat_mor_dat.close()


  def saveEveryNItr(self, itr, model, nb_obs, gb=None):
    fmodel_name = self.fmodel + str(itr) + '.sav'

    fh_model = open(fmodel_name, "wb")
    fh_touches = open(self.ftouches, "a+")

    pickle.dump(model, fh_model)
    fh_touches.write(str(nb_obs) + '\n')

    fh_model.close()
    fh_touches.close()
    if gb is not None:
      fgb_name = self.fgb + str(itr) + '.sav'
      fh_gb = open(fgb_name, "wb")
      pickle.dump(gb, fh_gb)
      fh_gb.close()

  def prepareTesting(self, itr):
   ftest_data_name = self.ftest_data + str(itr) + '.txt'
   ftest_ee_name = self.ftest_end_effector + str(itr) + '.txt'
   ftest_ma_name = self.ftest_motor_actual + str(itr) + '.txt'
   ftest_mt_name = self.ftest_motor_target + str(itr) + '.txt'
   ftest_3Dtt_name = self.ftest_3Dcoord_taxel_target + str(itr) + '.txt'
   self.fh_test = open(ftest_data_name, 'w')
   self.fh_test_ee = open(ftest_ee_name, 'w')
   self.fh_test_ma = open(ftest_ma_name, 'w')
   self.fh_test_mt = open(ftest_mt_name, 'w')
   self.fh_test_3Dtt = open(ftest_3Dtt_name, 'w')

  # t for target, a for actual feedback
  def saveTesting(self, tx, ty, ax, ay, dist, time_spent, mactual, mtarget, ee_pos, tt_pos):
    self.fh_test.write(str(tx) + ' ' + str(ty) + ' ' + str(ax) + ' ' + str(ay) + ' ' + str(dist) + ' ' + str(time_spent) + '\n')
    self.fh_test_ee.write(' '.join(map(str, ee_pos))+ '\n')
    self.fh_test_ma.write(' '.join(map(str, mactual))+ '\n')
    self.fh_test_mt.write(' '.join(map(str, mtarget))+ '\n')
    self.fh_test_3Dtt.write(str(tt_pos.x) + ' ' + str(tt_pos.y) + ' ' + str(tt_pos.z) + '\n')


  def endTesting(self):
   self.fh_test.close()
   self.fh_test_ee.close()
   self.fh_test_ma.close()
   self.fh_test_mt.close()
   self.fh_test_3Dtt.close()

