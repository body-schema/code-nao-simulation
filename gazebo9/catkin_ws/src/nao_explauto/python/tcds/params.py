# General imports
import numpy as np
class Params:

  def __init__(self, body_part, resolution, method, learning_itr, env_reset, scrots, no_hjoint):
   # Joint data structure
    self.joints = [
    {
      "joint_name": "RElbowRoll",
      "publisher": None,
      "limit_min": 0.0349066,
      "limit_max": 1.54462
    },
    {
      "joint_name": "RElbowYaw",
      "publisherg": None,
      "limit_min": -2.08567,
      "limit_max": 2.08567
    },
    {
      "joint_name": "RShoulderPitch",
      "publisher": None,
      "limit_min": -2.08567,
      "limit_max": 2.08567
    },
    {
      "joint_name": "RShoulderRoll",
      "publisher": None,
      "limit_min": -1.32645,
      "limit_max": 0.314159
    },
    {
      "joint_name": "RWristYaw",
      "publisher": None,
      "limit_min": -1.82387,
      "limit_max": 1.82387
    },
    ]

    self.actual_joint_pos = np.array([0.0, 0.0, 0.0, 0.0, 0.0])
    self.discarded_mtr_cmd = 0 # amount of cmds discarded (diff > partial_mtr_discard)
    self.ee_link_name = 'nao_skin::r_wrist' # End effector link name
    self.ee_link_state_name = "nao_skin::r_wrist_pen_point" # ee added link for 3D coordinates state
    self.error_pose = [9999.0, 9999.0, 9999.0, 9999.0, 9999.0]
    self.home_contact_mode = 1 # Home posture has a touch feedback (1) or no touch feedback (0)
    self.itr_explo = 0
    self.joints_range_dist = np.array([0.0, 0.0, 0.0, 0.0, 0.0])
    self.max_learning_iterations = learning_itr
    self.model_mode = 'exploit' # For explauto exploit: no added noise to mvt; explore: added noise
    self.nb_observation = 0 # Monitor amount of observations during exploration
    self.partial_mtr_discard = 0.1 # max allowed difference between target and actual joint config
    self.reached_taxels = [] # Reached taxels - for manual progress evaluation
    self.reference_frame = "world" # reference frame for 3D coordinates
    self.ros_topic = '/gazebo_contact_info/r_wrist' # ROS topic name for Gazebo contacts
    self.use_scrots = scrots # If True, take screenshots during test phase
    self.use_reset = env_reset # Defines if we want to reset simulation when applying motor commands

    # body part specific
    if body_part == "head":
      self.setupHeadParam(resolution, no_hjoint)
    else:
      self.setupTorsoParam(resolution)

    # Skin to coordinates mapping
    self.skin_map = {}
    map_fh = open(self.skin_name, 'r')
    if resolution == "high":
      for i in range(self.total_sensors):
        k = str(i + 1) + '_' + str(i + 2)
        _input = map_fh.readline().strip().split(" ")
        self.skin_map[k] = [float(x) for x in _input]
    else:
      for i in range(self.total_sensors):
        k = str(i) + '_' + str(i + 2)
        self.skin_map[k] = [float(x) for x in map_fh.readline().strip().split(" ")]
    map_fh.close()

    # Testing grid (torso; taxel indices)
    self.test_grid = []
    if resolution == 'high':
      map_tg = open(self.testing_grid_name, 'r')
      _input = map_tg.readline().strip().split(" ")
      self.test_grid = [int(x) for x in _input]
      map_tg.close()
    else:
      self.test_grid = [i for i in range(self.total_sensors)]

    # Method specific
    if method == "RGB":
      self.model_mode = 'explore'

  def setupHeadParam(self, resolution, no_hjoint):

    if resolution == "high":
      self.testing_grid_name = 'coordinate-maps/test-grid-highres-head.txt'
      self.total_sensors = 240 # Number of taxels
      self.skin_name = 'coordinate-maps/highres-head-planar.txt'
    if resolution == "low":
      self.testing_grid_name = 'coordinate-maps/lowres-head.txt'
      self.total_sensors = 24 # Number of taxels
      self.skin_name = 'coordinate-maps/lowres-head.txt'
    
    self.collision_target_link = 'Head' # Target link on which collisions are detected

    if no_hjoint:
      self.home_pose = [1.253, 0.541, -0.29, -0.1, 1.565]
      return
      
    self.actual_joint_pos = np.append(self.actual_joint_pos, [0.0, 0.0])
      
    ## Home posture
    # A nice choice (in contact with surface, generates an outcome)
    self.home_pose = [1.253, 0.541, -0.29, -0.1, 1.565, -0.08, 0.038] # middle mid head - right side
    # Far from surface (no outcome generated)
    #self.home_pose = [1.495, 1.539, 0.083, -0.02, 0.109, -0.21, -0.12] # trying to imitate baby posture
    # Old home pose - used for ICDL 2020 paper
    #self.home_pose = [1.117, 0.083, -0.87, -0.02, 1.638, -0.21, -0.12]

    self.joints_range_dist = np.append(self.joints_range_dist, [0.0, 0.0])

    self.error_pose.append(9999.0)
    self.error_pose.append(9999.0)

    self.joints.append({
      "joint_name": "HeadYaw",
      "publisher": None,
      "limit_min": -2.08567,
      "limit_max": 2.08567
    })
    self.joints.append({
      "joint_name": "HeadPitch",
      "publisher": None,
      "limit_min": -0.671952,
      "limit_max": 0.514872
    })

  def setupTorsoParam(self, resolution):
    self.collision_target_link = 'base_link' # Target link on which collisions are detected

    ## Home posture.

    # A nice choice (in contact with surface, generates an outcome)
    #self.home_pose = [1.525, 0.499, 0.541, -0.03, 0.764] # middle top torso - don't reliably touch skin on high res
    self.home_pose = [1.434, 0.125, 0.374, -0.1, 0.874] # middle mid torso
    # Far from surface (no outcome generated)
    #self.home_pose = [1.495, 1.539, 0.083, -0.02, 0.109, -0.21, -0.12] # trying to imitate baby posture
    # Old home pose - used for ICDL 2020 paper
    #self.home_pose = [1.48, -0.45, 0, -0.1, np.pi/2]

    if resolution == "high":
      self.testing_grid_name = 'coordinate-maps/test-grid-highres-torso.txt'
      self.total_sensors = 250 # Number of taxels
      self.skin_name = 'coordinate-maps/highres-torso-planar.txt'
    if resolution == "low":
      self.testing_grid_name = 'coordinate-maps/lowres-torso.txt'
      self.total_sensors = 25 # Number of taxels
      self.skin_name = 'coordinate-maps/lowres-torso.txt'
