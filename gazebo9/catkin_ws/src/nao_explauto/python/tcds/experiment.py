# General imports
import numpy as np
import os

# ROS and Gazebo imports
import rospy
from gazebo_link_pose import *
from gazebo_link_position import * # to get 3D coordinates of taxels, but doesn't work with python 3
from gazebo_msgs.srv import GetJointProperties, GetJointPropertiesResponse, GetLinkState
from nao_ml_inc import *
from std_msgs.msg import Float64

# Explauto/Environment imports
from explauto import Environment, SensorimotorModel, InterestModel
from explauto.interest_model.discrete_progress import DiscretizedProgress, competence_dist
from explauto.sensorimotor_model import sensorimotor_models, available_configurations
from explauto.sensorimotor_model.non_parametric import NonParametric
from nao_explauto_env_3 import NaoEnvironment

# Matthias' model/gb import
from gb import GoalBabbling
from models import LinearModel, LocalLinearModel

# classes for a functionnal script
from explogs import *# Log import
from params import *# Global (mostly hard-coded) params import

class Experiment:

  def __init__(self, body_part, resolution, method, learning_itr=1000, env_reset=False, scrots=True, no_hjoint=False):

    self.logs = Explogs()
    self.prm = Params(body_part, resolution, method, learning_itr, env_reset, scrots, no_hjoint)

    # Initialize ROS
    rospy.init_node('nao_skin_ml', anonymous=True)
    rospy.wait_for_service('/gazebo/get_joint_properties')
    self.srv_joint_state = rospy.ServiceProxy('/gazebo/get_joint_properties', GetJointProperties)
    rospy.wait_for_service('gazebo/get_link_state')
    self.state_getter = rospy.ServiceProxy('gazebo/get_link_state', GetLinkState)
    self.taxel_getter = GazeboLinkPosition("highres")

    # Initialize joints
    idx = 0
    for joint in self.prm.joints:
      topic_name = '/nao_dcm/' + joint['joint_name'] + '_position_controller/command';
      joint['publisher'] = rospy.Publisher(topic_name, Float64, queue_size=5)
      self.prm.joints_range_dist[idx] = abs(joint['limit_min'] - joint['limit_max'])
      self.prm.joints[idx] = joint
      idx += 1
    self.max_mtr_diff = np.multiply(self.prm.joints_range_dist, self.prm.partial_mtr_discard)

    # Initialize explauto environment
    self.joint_min = [x['limit_min'] for x in self.prm.joints]
    self.joint_max = [x['limit_max'] for x in self.prm.joints]

    # Set bounds of sensory space
    self.min_x = min([self.prm.skin_map[k][0] for k in self.prm.skin_map])
    self.max_x = max([self.prm.skin_map[k][0] for k in self.prm.skin_map])
    self.min_y = min([self.prm.skin_map[k][1] for k in self.prm.skin_map])
    self.max_y = max([self.prm.skin_map[k][1] for k in self.prm.skin_map])
    dx = (self.max_x - self.min_x)
    dy = (self.max_y - self.min_y)
    s_mins = [self.min_x - dx, self.min_y - dy]
    s_maxs = [self.max_x + dx, self.max_y + dy]

    # Create Nao environment (inheritence explauto's 'Environment' class)
    self.explauto_env = NaoEnvironment(self.joint_min, self.joint_max, s_mins, s_maxs, self.prm.joints, self.prm.ros_topic, self.prm.ee_link_name, self.prm.use_reset, self.prm.skin_map, self.prm.collision_target_link)

    # For highres skin, needed so that python can receive touch input from gazebo, because the simulation is slow
    # and explauto just reads the ROS topic, there is no synchronicity or flag to show that the topic was updated
    if resolution == "high":
      self.explauto_env.max_time_delay = 8.0
      self.explauto_env.time_delay_after = 3.0

    # set the reset to True to ensure correct initial command and bootstrapping
    self.explauto_env.use_reset = True
    print("Trying to reach home")
    self.home_se, timeSpent = self.explauto_env.compute_sensori_effect(self.prm.home_pose)
    self.startGoal = self.home_se
    print("Movement completed")

    self.initModel(method)

    self.explauto_env.compute_sensori_effect(self.prm.home_pose)
    self.explauto_env.use_reset = self.prm.use_reset

    # Initialization complete
    print("NAO Skin ML: Initialization complete")
    print('i: ' +  str(self.prm.itr_explo))

    self.processLoop(method, resolution, body_part)

  def initModel(self, method):
    if method == "PBGB":
      # Create targets for goal generation
      self.targetList = [] # List of goals for elephant/matthias algorithm
      for x in np.linspace(self.min_x, self.max_x, 10):
        for y in np.linspace(self.min_y, self.max_y, 10):
          self.targetList.append(np.array([x,y]))
      
      # Setup inverse model:
      M = len(self.prm.joints) # Output dimension (motor command)
      N = 2 # Input dimension (goal coordinates in 2d space)
      self.model = LocalLinearModel(N, M, dist=0.05)
      self.bootstrappingPBGB()
      # Setup exploration method
      goHomeProbability=0.1
      goalStepLength=0.01
      motorStepLength=0.1
      self.gb = GoalBabbling(self.explauto_env, self.model, self.prm.home_pose, self.targetList, startGoal=self.startGoal,
                  goHomeProbability=goHomeProbability, goalStepLength=goalStepLength,
                  motorStepLength=motorStepLength)
      self.gb.setMotorPerturbation(0.01)

      sample = self.gb.generateSample() #first time = generate home posture
    elif method == "DGB":
      self.sigma_explo_ratio = 0.05
      x_card = 1064 # 32*32
      win_size = 10
      # Instantiate a discretized progress goal interest model:
      self.im_model = DiscretizedProgress(self.explauto_env.conf, self.explauto_env.conf.s_dims, **{
        'x_card': x_card,
        'win_size': win_size,
        'measure': competence_dist,
        'eps_random':0.1
      })
      # Explauto model
      self.model = SensorimotorModel.from_configuration(self.explauto_env.conf, 'nearest_neighbor', 'default')
      self.bootstrappingExplautoGB()
    elif method == "RGB":
      # Instantiate a random goal interest model:
      self.im_model = InterestModel.from_configuration(self.explauto_env.conf, self.explauto_env.conf.s_dims, 'random')
      # Explauto model
      self.model = SensorimotorModel.from_configuration(self.explauto_env.conf, 'nearest_neighbor', 'default')
      self.bootstrappingExplautoGB()
    elif method == "MB":
      self.model = SensorimotorModel.from_configuration(self.explauto_env.conf, 'nearest_neighbor', 'default')
    elif method == "MBIM":
      self.sigma_explo_ratio = 0.05
      x_card = 1064 # 32*32
      win_size = 10
      # Instantiate a discretized progress goal interest model:
      self.im_model = DiscretizedProgress(self.explauto_env.conf, self.explauto_env.conf.m_dims, **{
        'x_card': x_card,
        'win_size': win_size,
        'measure': competence_dist,
        'eps_random':0.1
      })
      # Explauto model
      self.model = SensorimotorModel.from_configuration(self.explauto_env.conf, 'nearest_neighbor', 'default')
      self.bootstrappingExplautoGB()


  def bootstrappingExplautoGB(self):
    self.model.mode = 'exploit'
    s, time_spent = self.explauto_env.compute_sensori_effect(self.prm.home_pose)
    while self.prm.nb_observation < 10:
      m = nao_ml_get_random_goal(self.prm.home_pose, 1)
      s, time_spent = self.explauto_env.compute_sensori_effect(m)
      self.prm.itr_explo += 1
      if s is not None:
        self.model.update(m, s)
        self.prm.reached_taxels.append(s)
        print('Observation: ' + ','.join(str(x) for x in s))
        self.prm.nb_observation += 1
    self.model.mode = self.prm.model_mode

  def bootstrappingPBGB(self):
    print("Bootstrapping")
    if self.home_se is None and self.prm.home_contact_mode == 1:
      while self.home_se is None:
        self.home_se, timeSpent = self.explauto_env.compute_sensori_effect(self.prm.home_pose)
        self.startGoal = self.home_se
    if self.home_se is None:
      closestPosture = None
      closestOutcome = None
      closestDistance = float("Inf")
      itr = 0
      while itr < 100 or closestOutcome is None:
        randomMotorCommand = self.prm.home_pose + np.random.randn(M)*0.3
        np.maximum(randomMotorCommand, self.joint_min, randomMotorCommand)
        np.minimum(randomMotorCommand, self.joint_max, randomMotorCommand)
        outcome, timeSpent = self.explauto_env.compute_sensori_effect(randomMotorCommand)
        print(itr)
        if outcome is not None:
            d = np.linalg.norm(randomMotorCommand-self.prm.home_pose)
            print('Actual observation:   ' + ','.join(str(x) for x in outcome))
            if d < closestDistance:
                closestPosture = randomMotorCommand
                closestOutcome = outcome
                closestDistance = d
        itr += 1
      self.startGoal = closestOutcome
      self.model.adapt(closestOutcome, closestPosture, 1.0)
      self.model.offsets[0] = closestPosture
    else:
      self.model.adapt(self.home_se, self.prm.home_pose, 1.0)
      #model.offsets[0] = home_pose
    print("Home posture:" + str(self.prm.home_pose))
    print("Home position:" + str(self.home_se))
    print("Start goal:" + str(self.startGoal))

  # get Actual Motor Configuration
  def getAMC(self):
    joint_idx = 0
    flag_joint_reco_error = 0
    for joint in self.prm.joints:
      try:
        joint_response = self.srv_joint_state(joint['joint_name'])
        self.prm.actual_joint_pos[joint_idx] = joint_response.position[0]
        joint_idx += 1
      except:
        self.prm.actual_joint_pos[joint_idx] = 9999
        joint_idx = joint_idx + 1
        flag_joint_reco_error = 1
    return flag_joint_reco_error

    # get exploration State
  def getExploreState(self, starget, sactual, flag_joint_reco_error, time_spent):

    state_array = np.empty(9)
    state_array[0] = self.prm.itr_explo # iteration
    if starget is not None: # Intermediary goal in skin 2D space
      state_array[1] = starget[0]
      state_array[2] = starget[1]
    else:
      state_array[1] = 9999
      state_array[2] = 9999
    try: # End Effector 3D coordinates in gazebo space
      ee_link_response = self.state_getter(self.prm.ee_link_state_name, self.prm.reference_frame)
      ee_3D_position = ee_link_response.link_state.pose.position
      state_array[3] = ee_3D_position.x
      state_array[4] = ee_3D_position.y
      state_array[5] = ee_3D_position.z
    except rospy.ServiceException as e:
      print("Service call failed: %s"%e)
      state_array[3] = 9999
      state_array[4] = 9999
      state_array[5] = 9999
    if (sactual is not None) and (flag_joint_reco_error == 0): # Sensory feedback/touched taxel in skin 2D space
      state_array[6] = sactual[0]
      state_array[7] = sactual[1]
    else:
      state_array[6] = 9999
      state_array[7] = 9999
    state_array[8] = time_spent # time spent until ee was stable / end of movement
    return state_array

  # get additionnal Data related to elephant/matthas algorithm
  def getMatthiasMoreData(self):
    # Get the log state
    logState = self.gb.logState

    mat_mor_dat = np.empty(5)
    mat_mor_dat[0] = self.prm.itr_explo # iteration
    mat_mor_dat[1] = logState.weight # Learning rate weight coefficient
    if logState.endGoal is not None: # end goal in skin 2D space
        mat_mor_dat[2] = logState.endGoal[0]
        mat_mor_dat[3] = logState.endGoal[1]
    else:
      if self.startGoal is not None:
        mat_mor_dat[2] = self.startGoal[0]
        mat_mor_dat[3] = self.startGoal[1]
      else:
        mat_mor_dat[2] = 9999
        mat_mor_dat[3] = 9999
    mat_mor_dat[4] = logState.endGoalChangeNext #Flag for class transition next iteration
    return mat_mor_dat, logState.className

  def processLoop(self, method, resolution, body_part):
    while (not rospy.is_shutdown()) and (self.prm.itr_explo < self.prm.max_learning_iterations):
      self.prm.itr_explo += 1
      # Generate goal
      class_name = None
      mat_mor_dat = None
      starget = None
      if method == "PBGB":
        sample = self.gb.generateSample()
        starget = sample.target
        sactual = sample.outcome
        time_spent = sample.timeSpent
        motor_cmd = sample.motor
        mat_mor_dat, class_name = self.getMatthiasMoreData()
      elif method == "DGB":
        starget = self.im_model.sample()
        # Infer a motor command to reach that goal using the sensorimotor model:
        motor_cmd = self.model.model.infer_order(tuple(starget))
        motor_cmd = np.random.normal(motor_cmd, self.sigma_explo_ratio) #Add exploration noise
        # Execute this command and observe the corresponding sensory effect:
        sactual, time_spent = self.explauto_env.compute_sensori_effect(motor_cmd)
      elif method == "RGB":
        starget = self.im_model.sample()
        # Infer a motor command to reach that goal using the sensorimotor model:
        motor_cmd = self.model.inverse_prediction(starget)
        sactual, time_spent = self.explauto_env.compute_sensori_effect(motor_cmd)
      elif method == "MB":
        motor_cmd = self.explauto_env.random_motors(n = 1)
        if body_part == "torso":
          motor_cmd = [motor_cmd[0][0], motor_cmd[0][1], motor_cmd[0][2], motor_cmd[0][3], motor_cmd[0][4]]
        else:
          motor_cmd = [motor_cmd[0][0], motor_cmd[0][1], motor_cmd[0][2], motor_cmd[0][3], motor_cmd[0][4], motor_cmd[0][5], motor_cmd[0][6]]
        sactual, time_spent = self.explauto_env.compute_sensori_effect(motor_cmd)
      elif method == "MBIM":
        motor_cmd = self.im_model.sample() # 'target' motor command
        # Make sensory prediction
        starget = self.model.forward_prediction(motor_cmd)
        sactual, time_spent = self.explauto_env.compute_sensori_effect(motor_cmd)


      # Get actual motor configuration
      flag_joint_reco_error = self.getAMC()
      # Get actual motor configuration
      state_array = self.getExploreState(starget, sactual, flag_joint_reco_error, time_spent)
      self.logs.saveEveryItr(self.prm.actual_joint_pos, state_array, motor_cmd, mat_mor_dat, class_name)

      # defaults to using actual joint value instead of target mtr cmd
      is_above_max_diff = True
      if flag_joint_reco_error == 0:
        diff = abs(np.subtract(motor_cmd, self.prm.actual_joint_pos))
        diff_above_accepted = diff > self.max_mtr_diff
        is_above_max_diff = np.any(diff_above_accepted == True)
      if is_above_max_diff:
        print("Motor command discarded because values are too much outside one of the joint's range.")
        print('Model is not updated.')
        self.prm.discarded_mtr_cmd += 1

      if starget is not None:
        print('Target sensory goal: ' + ','.join(str(x) for x in starget))
      print('Target motor command: ', self.prm.itr_explo, [round(x, 2) for x in motor_cmd])
      if sactual is None:
        print('Actual sensory feedback: None')
      elif not is_above_max_diff:
        if method == "PBGB":
          self.model.adapt(sactual, motor_cmd, 1 * sample.weight)
        else:
          self.model.update(motor_cmd, sactual)
          if (method == "DGB") or (method == "MBIM"):
            self.im_model.update(np.hstack((motor_cmd, starget)), np.hstack((motor_cmd, sactual)))
        self.prm.reached_taxels.append(sactual)
        self.prm.nb_observation += 1
        print('Actual sensory feedback: ' + ','.join(str(x) for x in sactual))
      else:
        print('Actual sensory feedback: ' + ','.join(str(x) for x in sactual))
      print('Actual motor configuration: ' + ','.join(str(x) for x in self.prm.actual_joint_pos))
      print('')

      # Save every 100 or 1000 iterations:
      if (self.prm.itr_explo < 1000 and self.prm.itr_explo % 100 == 0) or (self.prm.itr_explo % 1000 == 0):
        if method == "PBGB":
          # Can't save self.gb because of a pickle thread lock error
          self.logs.saveEveryNItr(self.prm.itr_explo, self.model, self.prm.nb_observation)
        elif (method == "DGB") or (method == "MBIM"):
          self.logs.saveEveryNItr(self.prm.itr_explo, self.model, self.prm.nb_observation, self.im_model)
        elif method == "RGB" or method == "MB":
          self.logs.saveEveryNItr(self.prm.itr_explo, self.model, self.prm.nb_observation)
        self.testModel(method, resolution, body_part)

      #reset every 100 iterations just in case to avoid stuck situations
      if (self.prm.itr_explo % 100 == 0) and (self.explauto_env.use_reset == False):
        self.explauto_env.use_reset = True
        self.explauto_env.compute_sensori_effect(self.prm.home_pose)
        self.explauto_env.compute_sensori_effect(self.prm.actual_joint_pos)
        self.explauto_env.use_reset = self.prm.use_reset
        
    self.logs.saveEndExp(self.prm.reached_taxels, self.prm.discarded_mtr_cmd)

  def loadModel(self, itr, method):
    # Load saved model from disk
    filename = 'models/model-' + str(itr) + '.sav'
    fh = open(filename, 'rb')
    self.model = pickle.load(fh)
    fh.close()
    self.prm.itr_explo = itr
    if (method == "DGB") or (method == "MBIM"):
      filename = 'models/gb-' + str(itr) + '.sav'
      fh = open(filename, 'rb')
      self.im_model = pickle.load(fh)
      fh.close()

  def screenshot(self, scrot_filename):
    # Takes a screenshot of the screen.
    # if -u: only screenshots the current active window instead of the whole screen
    # Screenshots will be a black screen if the computer is locked, but they work
    # correctly if the screen is unplugged 
    if self.prm.use_scrots:
      sys_cmd = "scrot " + scrot_filename + " -u"
      os.system(sys_cmd)
    return

  # Get end effector position during testing
  def testEEPos(self):
    try: # End Effector 3D coordinates in gazebo space
      ee_link_response = self.state_getter(self.prm.ee_link_state_name, self.prm.reference_frame)
      ee_3D_position = ee_link_response.link_state.pose.position
      ee_pos = np.array([0.0, 0.0, 0.0])
      ee_pos[0] = ee_3D_position.x
      ee_pos[1] = ee_3D_position.y
      ee_pos[2] = ee_3D_position.z
    except rospy.ServiceException as e:
      print("Service call failed: %s"%e)
      ee_pos[0] = 9999
      ee_pos[1] = 9999
      ee_pos[2] = 9999
    return ee_pos
    

  def testModel(self, method, resolution, body_part):
    # Reset the simul for more accurate evaluation of the command performance
    self.explauto_env.use_reset = True
    if method == "RGB":
      self.model.mode = 'exploit'
    self.logs.prepareTesting(self.prm.itr_explo)
    print('Test learnt model')
    for k in self.prm.test_grid:
      scrot_filename = "output/scrots/data-" + str(self.prm.itr_explo) + "_taxel-" + str(k) + ".png"
      if resolution == "high":
        k_str = str(k) + '_' + str(k+1)
      else:
        k_str = str(k) + '_' + str(k+2)
      s_g = self.prm.skin_map[k_str]
      s = None
      best_dist = float("inf")
      # Best of 2, just to be sure; should always be the same m but execution may change
      for kk in range(2):
        try:
          if method == "PBGB":
            m = self.model.predict(s_g)
          else:
            m = self.model.inverse_prediction(s_g)
          s_t, time_spent = self.explauto_env.compute_sensori_effect(m)
        except:
          m = self.prm.error_pose
          s_t = None
          time_spent = 0.0
        flag_joint_reco_error = self.getAMC()
        ee_pos = self.testEEPos()
        if body_part == "torso":
          tt_pos = self.taxel_getter.get_taxel_position("torso", k-1)
        else:
          tt_pos = self.taxel_getter.get_taxel_position("head", k-1)
        if s_t is not None:
          dist = ((s_g[0] - s_t[0])**2 + (s_g[1] - s_t[1])**2)**0.5
          if dist < best_dist:
            self.screenshot(scrot_filename) 
            s = s_t[:]
            best_dist = dist
            best_mt = m
            best_ma = self.prm.actual_joint_pos
            best_time = time_spent
            best_eepos = ee_pos
            best_ttpos = tt_pos
      print('Expected observation: ' + ','.join(str(x) for x in s_g))
      if s is None:
        print('Actual observation:   None')
        self.screenshot(scrot_filename)
        self.logs.saveTesting(s_g[0], s_g[1], 9999, 9999, 9999, time_spent, self.prm.actual_joint_pos, m, ee_pos, tt_pos)
      else:
        print('Actual observation:   ' + ','.join(str(x) for x in s))
        print('Distance:   ' + str(best_dist))
        self.logs.saveTesting(s_g[0], s_g[1], s[0], s[1], best_dist, best_time, best_ma, best_mt, best_eepos, best_ttpos)
      print('')
    self.logs.endTesting()
    # reset values back to whatever it should be during exploration
    self.explauto_env.use_reset = self.prm.use_reset
    if method == "RGB":
      self.model.mode = self.prm.model_mode

