#!/bin/bash

# if virtualenvwrapper.sh is in your PATH (i.e. installed with pip)
# source to make it usable in the script
source `which virtualenvwrapper.sh`
workon gazebop3

# Variables: to change for each set of experiment to run
experiment="./exp0405-dgb-planar-htorso-rhand.py"
experiment2="./exp0403-rgb-planar-htorso-rhand.py"
expNB="exp0405"
expNB2="exp0403"

# Create the directory for the expriment
mkdir -p results_stats/${expNB}


for i in {1..5}
#for i in 1
  do

  # Create the directory for the run
  mkdir -p results_stats/${expNB}/${expNB}_${i}
  mkdir -p output
  mkdir -p output/scrots
  mkdir -p models
  
  
  SECONDS=0
  python ${experiment}
  elapsedseconds=$SECONDS
  
  echo "$(($elapsedseconds / 3600)):$((($elapsedseconds / 60) % 60)):$(($elapsedseconds % 60))" > output/real_time_spent.txt

  # Move the results from the default folder to the
  # experiment-specific folder
  mv output results_stats/${expNB}/${expNB}_${i}/
  mv models results_stats/${expNB}/${expNB}_${i}/
done

for i in {1..5}
#for i in 2
  do

  # Create the directory for the run
  mkdir -p results_stats/${expNB2}/${expNB2}_${i}
  mkdir -p output
  mkdir -p output/scrots
  mkdir -p models
  
  
  SECONDS=0
  python ${experiment2}
  elapsedseconds=$SECONDS
  
  echo "$(($elapsedseconds / 3600)):$((($elapsedseconds / 60) % 60)):$(($elapsedseconds % 60))" > output/real_time_spent.txt

  # Move the results from the default folder to the
  # experiment-specific folder
  mv output results_stats/${expNB2}/${expNB2}_${i}/
  mv models results_stats/${expNB2}/${expNB2}_${i}/
done
