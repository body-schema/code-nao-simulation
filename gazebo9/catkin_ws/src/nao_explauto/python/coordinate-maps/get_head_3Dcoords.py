from gazebo_link_position import *
import numpy as np

if __name__ == "__main__":
  getter = GazeboLinkPosition("highres") # Parameter should be "lowres" or "highres"
  fn_head = "head_highres_3D_gazebo.txt"
  fh_head = open(fn_head, 'a+')
  
for i in range(240):
  #print (i)
  p = getter.get_taxel_position("head", i)
  #fh_head.write("%.20f %.20f %.20f\n" % (p.x, p.y, p.z))
  fh_head.write(str(p.x) + ' ' + str(p.y) + ' ' + str(p.z) + '\n')
  #print(p)

fh_head.close()
