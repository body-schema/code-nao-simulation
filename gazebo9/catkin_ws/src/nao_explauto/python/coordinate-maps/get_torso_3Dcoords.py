from gazebo_link_position import *
import numpy as np

if __name__ == "__main__":
  getter = GazeboLinkPosition("highres") # Parameter should be "lowres" or "highres"
  fn_torso = "torso_highres_3D_gazebo.txt"
  fh_torso = open(fn_torso, 'a+')
  
for i in range(250):
  #print (i)
  p = getter.get_taxel_position("torso", i)
  #fh_torso.write("%.20f %.20f %.20f\n" % (p.x, p.y, p.z))
  fh_torso.write(str(p.x) + ' ' + str(p.y) + ' ' + str(p.z) + '\n')
  #print(p)

fh_torso.close()
