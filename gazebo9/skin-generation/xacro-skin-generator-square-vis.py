import numpy as np
from placeSensors import *

VERTICES = [
    "coordinates/torso.txt",
    "coordinates/head.txt",
    "coordinates/left-arm.txt",
    "coordinates/right-arm.txt",
]
SENSOR_SIZE = "0.0151 0.0151 0.0011"
SENSOR_NAMES = [
    "skin_torso",
    "skin_head",
    "skin_l_wrist",
    "skin_r_wrist",
]
LINK_NAMES = [
    "torso",
    "Head",
    "l_wrist",
    "r_wrist",
]
ARG_NAMES = [
    "contact_torso",
    "contact_head",
    "contact_left_arm",
    "contact_right_arm",
]
XYZ_OFFSETS = [
    [0, 0, 0.01],
    [0, 0, 0.01],
    [-0.025, 0, 0],
    [-0.025, 0, 0]
]

with open('link-template-square-vis.urdf.txt', 'r') as fid:
    linkTemplate = fid.read()

print('<?xml version="1.0" ?>')
print('<robot xmlns:xacro="http://www.ros.org/wiki/xacro">')

for i, file in enumerate(VERTICES):
    vertices = np.loadtxt(file)
    print('    <xacro:macro name="insert_skin_' + LINK_NAMES[i] + '_coords">')
    for j, vertex in enumerate(vertices):
        name = LINK_NAMES[i] + '_skin_coords_' + str(j)
        center = vertex[0:3] + XYZ_OFFSETS[i]
        normal = vertex[3:6]
        rpy = calcRPY(np.array([0, 0, 1]), normal)

        child_name = name
        parent_name = LINK_NAMES[i]
        joint_name = name + '_joint'

        print(
            linkTemplate
                .replace("SENSOR_XYZ", str(center)[1:-1])
                .replace("SENSOR_RPY", str(rpy)[1:-1])
                .replace("SENSOR_SIZE", str(SENSOR_SIZE))
                .replace("CHILD_LINK_NAME", child_name)
                .replace("PARENT_LINK_NAME", parent_name)
                .replace("JOINT_NAME", joint_name)
        )
    print('    </xacro:macro>')
    
print('</robot>')
