# This version places all collision elements as direct children of casing element
# Coordinates are computed here in the script using RPY angles and rotation matrices
# Status: working

import numpy as np
from placeSensors import *

def rotz(x):
    c = np.cos(x)
    s = np.sin(x)
    R = np.array(((c, -s, 0), (s, c, 0), (0, 0, 1)))
    return R
def roty(x):
    c = np.cos(x)
    s = np.sin(x)
    R = np.array(((c, 0, s), (0, 1, 0), (-s, 0, c)))
    return R
def rotx(x):
    c = np.cos(x)
    s = np.sin(x)
    R = np.array(((1, 0, 0), (0, c, -s), (0, s, c)))
    return R
def rpy2rot(rpy):
    return np.matmul(np.matmul(rotz(rpy[2]), roty(rpy[1])), rotx(rpy[0]))

VERTICES = [
    "coordinates/torso.txt",
    "coordinates/head.txt",
    "coordinates/left-arm.txt",
    "coordinates/right-arm.txt",
]
SENSOR_LENGTH = "0.0011"
SENSOR_RADIUS = "0.0023"
SENSOR_NAMES = [
    "skin_torso",
    "skin_head",
    "skin_l_wrist",
    "skin_r_wrist",
]
LINK_NAMES = [
    "torso",
    "Head",
    "l_wrist",
    "r_wrist",
]
ARG_NAMES = [
    "contact_torso",
    "contact_head",
    "contact_left_arm",
    "contact_right_arm",
]
sensors_per_link = 10 
sensor_offsets = [
    [0, 0.006533, 0],
    [-0.00566, 0.0098, 0],
    [-0.00566, 0.003267, 0],
    [0, 0, 0],
    [-0.00566, -0.003267, 0],
    [-0.00566, -0.0098, 0],
    [0, -0.006533, 0],
    [0.00566, -0.003267, 0],
    [0.011317, 0, 0],
    [0.00566, 0.003627, 0],
]
XYZ_OFFSETS = [
    [0, 0, 0.01],
    [0, 0, 0.01],
    [-0.025, 0, 0],
    [-0.025, 0, 0]
]

with open('link-template-triangle-2-vis.urdf.txt', 'r') as fid:
    linkTemplate = fid.read()

print('<?xml version="1.0" ?>')
print('<robot xmlns:xacro="http://www.ros.org/wiki/xacro">')

for i, file in enumerate(VERTICES):
    vertices = np.loadtxt(file)
    ind = 0
    print('    <xacro:macro name="insert_skin_' + LINK_NAMES[i] + '_coords">')
    for j, vertex in enumerate(vertices):
        center = vertex[0:3] + XYZ_OFFSETS[i]
        normal = vertex[3:6]
        rpy = calcRPY(np.array([0,0,1]), normal)
        
        for k in range(sensors_per_link):
            ind += 1
            name = LINK_NAMES[i] + '_skin_coords_' + str(ind)
            child_name = name
            parent_name = LINK_NAMES[i]
            joint_name = name + '_joint'

            yaw_correction = vertex[6]

            rot_matrix = rpy2rot(rpy)
            center_k = np.add(center, np.matmul(rot_matrix, np.matmul(rotz(yaw_correction), sensor_offsets[k])))

            print(
                linkTemplate
                    .replace("SENSOR_XYZ", str(center_k)[1:-1])	
                    .replace("SENSOR_RPY", str(rpy)[1:-1])
                    .replace("SENSOR_LENGTH", str(SENSOR_LENGTH))
                    .replace("SENSOR_RADIUS", str(SENSOR_RADIUS))
                    .replace("CHILD_LINK_NAME", child_name)
                    .replace("PARENT_LINK_NAME", parent_name)
                    .replace("JOINT_NAME", joint_name)
            )

    print('    </xacro:macro>')

print('</robot>')
